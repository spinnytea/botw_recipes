'use strict';

// return require('./data/scripts/scrape_ign')
// 	.process('./data/raw/ign', './data/raw')
// 	.then((data)=>console.log(data), (err)=>console.error(err));

// I think this needs to be run first
// return require('./data/scripts/scrape_orcz')
// 	.process('./data/raw/orcz', './data/raw')
// 	.then((data)=>console.log(data), (err)=>console.error(err));

// return require('./data/scripts/scrape_fandom_dishes')
// 	.process('./data/raw/fandom', './data/raw')
// 	.then((data)=>console.log(data), (err)=>console.error(err));

// return require('./data/scripts/scrape_fandom_ingredients')
// 	.process('./data/raw/fandom', './data/raw')
// 	.then((data)=>console.log(data), (err)=>console.error(err));

// combined needs to be tweaked, then this can run, then combined can run plain
// return require('./data/scripts/scrape_images')
// 	.process('./data/raw/images', './botw_recipes_vue/src/assets', './botw_recipes_vue/public')
// 	.then((data)=>console.log(data), (err)=>console.error(err));

require('./data/scripts/combine_scrapes')
	.process('./data/raw', './botw_recipes_vue/src/assets')
	.then((data)=>console.log(data), (err)=>console.error(err));
