module.exports = {
	transpileDependencies: ['vuetify'],
	productionSourceMap: false,
	publicPath: process.env.NODE_ENV === 'production' ? '/botw_recipes/' : '/',
};
