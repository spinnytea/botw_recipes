'use strict';
import database from './assets/combined.json';
import Dish from './data-classes/Dish';
import Effect from './data-classes/Effect';
import Food from './data-classes/Food';

const {
	effects: effectsImport,
	food: foodImport,
	dishes: dishesImport,
	anys,
	anysText,
}: {
	dishes: {
		name: string;
		examples: {
			name: string;
			ingredients: string[];
			notes?: string;
			effect: string | null;
		}[];
		category: string;
		method: string;
		localImage: string;
		generic?: string[];
		ingredients?: string[];
	}[];
	food: {
		name: string;
		number: number;
		hearts: number;
		effect: string;
		potency: string;
		duration: string;
		localImage: string;
		found: string[];
	}[];
	effects: {
		name: string;
		maxPotency: number;
		description: string;
		localImage: string;
	}[];
	anys: { [key: string]: string[] };
	anysText: { [key: string]: string[] };
} = database;

const effectsList = effectsImport.map(e => new Effect(e));
const effectsMap = effectsList.reduce((ret: { [key: string]: Effect }, effect) => {
	ret[effect.name] = effect;
	return ret;
}, {});

const foodList = foodImport.map(f => new Food(f, effectsMap[f.effect]));
const foodMap = foodList.reduce((ret: { [key: string]: Food }, food) => {
	ret[food.name] = food;
	return ret;
}, {});

// TODO make this automated
anys['Any variety of four different fish'] = anys['Any fish'];
anys['Any variety of four different meats'] = anys['Any meat'];
anys['Any variety of four different mushrooms'] = anys['Any mushroom'];
anys['Any variety of four different fruits'] = anys['Any fruit'];
anys['Any variety of four different vegetables or herbs'] = anys['Any vegetable or herb'];

const dishes = dishesImport.map(f => new Dish(f, anys, anysText, effectsMap, foodMap));

export { dishes, foodList, foodMap, effectsList, effectsMap, anys };
