import { flatten, uniq } from 'lodash';
import Effect from './Effect';
import Example from './Example';
import Food from './Food';

function removeSpecial(str: string) {
	str = str
		.replace(/-/g, ' ')
		.replace(/&/g, 'and')
		.replace(/[éè]/g, 'e');
	if (!/^[a-zA-Z ]+$/.test(str)) throw new Error(str + ' still has special characters');
	return str;
}

export default class Dish {
	readonly name: string;
	readonly examples: Example[];
	readonly category: string;
	readonly method: 'Cooking' | 'Roasting' | 'Freezing' | 'Boiling';
	readonly localImage: string;
	readonly generic?: string[];
	readonly ingredients?: Food[];
	// computed
	readonly $text: string;
	readonly $possibleIngredients: string[];

	constructor(
		json: any,
		anys: { [key: string]: string[] },
		anysText: { [key: string]: string[] },
		effectsMap: { [key: string]: Effect },
		foodMap: { [key: string]: Food },
	) {
		this.name = json.name;
		this.examples = json.examples.map((e: any) => new Example(e, effectsMap, foodMap));
		this.category = json.category;
		this.method = json.method;
		this.localImage = json.localImage;

		if (json.generic) {
			this.generic = json.generic;
			this.$possibleIngredients = flatten(json.generic.map((g: string) => (g.startsWith('Any ') ? anys[g] : g)));
		}
		// otherise ingredients
		else {
			this.ingredients = json.ingredients.map((i: string) => foodMap[i]);
			this.$possibleIngredients = json.ingredients; // just the names
		}

		// compute all the text strings we want to search by, combine into one long '|' dilimited string
		// name, method, category
		const textList: string[] = [this.name, removeSpecial(this.name), this.method, this.category];
		// ingredient names
		Array.prototype.push.apply(textList, this.$possibleIngredients);
		// instead of using the generic list directly, us the curated text version instead
		if (this.generic) {
			Array.prototype.push.apply(
				textList,
				this.generic.map(g => anysText[g]),
			);
		}
		this.examples.forEach(e => {
			if (e.name !== this.name) textList.push(e.name); // we are using uniq below, but this is trival to check now
			if (e.notes) textList.push(e.notes);
			if (e.effect) textList.push(e.effect.name);
			if (e.effect) textList.push(e.effect.description);
		});
		this.$text = uniq(textList)
			.join('|')
			.toLowerCase();
	}
}
