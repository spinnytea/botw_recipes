export default class Effect {
	readonly name: string;
	readonly description: string;
	readonly localImage: string;

	constructor(json: any) {
		this.name = json.name;
		this.description = json.description;
		this.localImage = json.localImage;
	}
}
