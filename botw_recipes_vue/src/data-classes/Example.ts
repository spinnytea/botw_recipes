import Effect from './Effect';
import Food from './Food';

export default class Example {
	readonly name: string;
	readonly ingredients: Food[];
	readonly notes?: string;
	readonly effect: Effect | null;

	constructor(json: any, effectsMap: { [key: string]: Effect }, foodMap: { [key: string]: Food }) {
		this.name = json.name;
		this.ingredients = json.ingredients.map((i: string) => foodMap[i]);
		this.notes = json.notes;
		if (json.effect) {
			this.effect = effectsMap[json.effect];
		}
		// we want to omit effect, rather than show a default value
		else {
			this.effect = null;
		}
	}
}
