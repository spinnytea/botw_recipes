import Effect from './Effect';
import { padStart } from 'lodash';

export default class Food {
	readonly name: string;
	readonly number: string;
	readonly hearts: number;
	readonly effect: Effect;
	readonly potency: string;
	readonly duration: string;
	readonly localImage: string;
	readonly found: string[];

	constructor(json: any, effect: Effect | undefined) {
		if (effect === undefined) effect = new Effect({ name: 'None', description: 'None' });

		this.name = json.name;
		this.number = padStart(json.number, 3, '0');
		this.hearts = json.hearts;
		this.effect = effect;
		this.potency = json.potency.replace(/\//g, ' / ');
		this.duration = json.duration;
		this.localImage = json.localImage;
		this.found = json.found;
	}
}
