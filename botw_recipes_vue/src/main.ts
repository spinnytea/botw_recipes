import Vue from 'vue';
import App from './App.vue';
import vuetify from './plugins/vuetify';
import 'reflect-metadata';

Vue.config.productionTip = false;

const app = new Vue({
	vuetify,
	render: h => h(App),
});
app.$mount('#app');
// if (process.env.NODE_ENV !== 'production') window.$app = app;
