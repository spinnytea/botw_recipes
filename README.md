Recipes from Zelda: Breath of the Wild
======================================

BotW has things to cook. Lots of things to cook. So many things to cook, in fact, that it's hard to remember them all.

You can throw just about anything into a pot and something will come out. And there's a general rule system about making buffs and healing items. But this is about the flavor, the end dish. When you want to have the crab soup or stuffed pumpkin. When you want to look at your Food menu and drool.


Goals
---

* Pull more information about ingredients
  * what they are
  * what are the ingredients
  * where to find ingredients
* able to search for dishes
  * no need to build based on selected ingredients; there good sites for that
  * not just pick dishes from a list, but actually search for them
  * a comprehensive list of dishes
* embed video
  * youtube query results? about how to make each dish
  * select videos by hand, embed those?
* real-life recipes for the dishes

Tasks
---

* Fetch More recipes
  * orcz -> food -> type (combine with anys)
  * fetch in-game recipe descriptions?
  * If food locations are a priority, then we need to get more sources
  * Looks like fandom no longer has data, and IGN updated their site format
* Review
  * ? All words filter: split on quotes and combine (1 "2 3" "4 5" ...)


Resources
---

### Inspiration

Of course I had my own original vision, and these are close but not quite. Invariably, they given me some ideas that I can't unsee.

[Guide of the Wild](https://www.guideofthewild.com/cook)

[BotW Recipes](http://botw-recipes.com/)

### Data

There are a ton of recipes, and images, and locations, and I would have never done this if I couldn't have data-mined my info. Only some of it is entered by my hand in situations where the data was splotchy.

[IGN: All Recipes](https://www.ign.com/wikis/the-legend-of-zelda-breath-of-the-wild/All_Recipes_and_Cookbook)

[ORCZ: Recipe](http://orcz.com/Breath_of_the_Wild:_Recipes)

[Zelda Fandom: Dishes](https://zelda.fandom.com/wiki/Category:Dishes)

Calamity Sans - I've had it for a while, not sure where I got it. Probably [reddit](https://www.reddit.com/r/zelda/comments/5txuba/breath_of_the_wild_ui_font/).

[Hylia Serif](https://zeldauniverse.net/media/fonts/)
