/**
 * this was the first source, it was easy to get to, but I don't actually like it all that much
 * the urls are hard to get to
 * the image urls are 1-offs
 * the images are quick screengrabs, not original/cleaned up versions
 *
 * the main takeaway from this is the 'categories' for whatever that's worth
 * it is however a good place to cross reference whatever we happen to have from it
 */
'use strict';
const _ = require('lodash');
const cheerio = require('cheerio');
const path = require('path');
const lp = require('./load_page');
const utils = require('./utils');

// xBUGx IGN returns a 403 when we request it programmatically
//  - maybe it's something to do with request headers
//  - maybe it's something to do with https and we can't just do that out of the blue
var IGN_URL = 'https://www.ign.com/wikis/the-legend-of-zelda-breath-of-the-wild/All_Recipes_and_Cookbook';

// gotta check anything you parse, anything that could go wrong because the website updated
// XXX tableIdx is an input/config, not a check; but i guess it IS cross checked when we verify the count and items
//  - still, it would be better if there was a better selector we could use
//  - instead, this just says "find all the tables on the page, and then use the one at this index"
var CHECKS = {
	'Vegetarian Entrees': {
		tableIdx: 2,
		count: 26,
		firstItem: {
			category: 'Vegetarian Entrees',
			name: 'Creamy Heart Soup',
			generic: ['Any Hearty Radish or Big Hearty Radish', 'Hydromelon', 'Voltfruit', 'Fresh Milk'],
		},
		lastItemName: 'Omelet',
	},
	'Seafood Entrees': {
		tableIdx: 3,
		count: 23,
		firstItem: {
			category: 'Seafood Entrees',
			name: 'Hearty Clam Chowder',
			ingredients: ['Hearty Blueshell Snail', 'Goat Butter', 'Fresh Milk', 'Tabantha Wheat'],
		},
		lastItemName: 'Fish Skewer',
	},
	'Poultry and Meat Entrees': {
		tableIdx: 4,
		count: 31,
		firstItem: {
			category: 'Poultry and Meat Entrees',
			name: 'Gourmet Meat Stew',
			generic: ['Any gourmet meat', 'Fresh Milk', 'Goat Butter', 'Tabantha Wheat'],
		},
		lastItemName: 'Meat Skewer',
	},
	'Surf and Turf Entrees': {
		tableIdx: 5,
		count: 4,
		firstItem: {
			category: 'Surf and Turf Entrees',
			name: 'Meat and Seafood Fry',
			generic: ['Any basic meat', 'Any seafood'],
		},
		lastItemName: 'Spicy Meat and Seafood Fry',
	},
	'Desserts': {
		tableIdx: 6,
		count: 16,
		firstItem: {
			category: 'Desserts',
			name: 'Apple Pie',
			ingredients: ['Apple', 'Goat Butter', 'Cane Sugar', 'Tabantha Wheat'],
		},
		lastItemName: 'Energizing Honey Candy',
	},
	'Monster Foods': {
		tableIdx: 7,
		count: 5,
		firstItem: {
			category: 'Monster Foods',
			name: 'Monster Soup',
			ingredients: ['Monster Extract', 'Goat Butter', 'Fresh Milk', 'Tabantha Wheat'],
		},
		lastItemName: 'Monster Cake',
	},
	'Sides and Snacks': {
		tableIdx: 8,
		count: 13,
		firstItem: {
			category: 'Sides and Snacks',
			name: 'Dubious Food',
			ingredients: ['Two or more items with effect', 'Monster parts', 'Generally incompatible items'],
		},
		lastItemName: 'Glazed Veggies',
	},
};

/**
 * e.g. process('./data/raw/ign', './data/raw')
 */
exports.process = function (path_to_raw, path_to_data) {
	const list = [];
	let food = [];

	return lp.loadJson(path.join(path_to_data, 'orcz.json')).then(function (database) {
		food = database.food;
	}).then(function () {
		return lp(path.join(path_to_raw, 'cookbook.html'), IGN_URL)
	}).then(function (page) {
		const $ = cheerio.load(page);

		// verify page
		utils.verifyCount($('body'), 1, 'dummy check: body');
		utils.verifyCount($('.wiki-page'), 1, 'dummy check: wiki');

		// categories
		utils.verifyCount($('.wiki-section > h2 > .mw-headline'), 4, 'dummy check: top categories');
		utils.verifyText($('.wiki-section > h2 > .mw-headline'), ['Entrees', 'Desserts', 'Monster Foods', 'Sides and Snacks']);
		// entree categories
		utils.verifyCount($('.wiki-section > h3 > .mw-headline'), 4, 'dummy check: entree categories');
		utils.verifyText($('.wiki-section > h3 > .mw-headline'), ['Vegetarian Entrees', 'Seafood Entrees', 'Poultry and Meat Entrees', 'Surf and Turf Entrees']);

		// table[0] = category links
		// table[1] = entry category links
		const $tables = $('.wiki-section > table');
		utils.verifyCount($tables, 9, 'dummy check: tables');
		parseCategoryTable($tables, 'Vegetarian Entrees');
		parseCategoryTable($tables, 'Seafood Entrees');
		parseCategoryTable($tables, 'Poultry and Meat Entrees');
		parseCategoryTable($tables, 'Surf and Turf Entrees');
		parseCategoryTable($tables, 'Desserts');
		parseCategoryTable($tables, 'Monster Foods');
		parseCategoryTable($tables, 'Sides and Snacks');
	}).then(function () {
		return lp.saveJson(_.sortBy(list, 'name'), path.join(path_to_data, 'ign.json'));
	}).then(function () {
		return 'IGN Done.';
	});

	function parseCategoryTable($tables, category) {
		const checks = CHECKS[category];
		if(!checks) return console.log('IGN '  + category + ' coming soon...');
		const $rows = cheerio($tables[checks.tableIdx]).find('tr');
		utils.verifyCount($rows, checks.count + 1, category + ': table rows in ' + category);

		$rows.each((idx, el) => {
			el = cheerio(el);
			if(idx === 0) {
				// each category has a different word ...
				// verifyText(el.find('td'), ['Meal', 'Ingredients']);
				return;
			}

			const left = cheerio(el.find('td')[0]);
			const right = cheerio(el.find('td')[1]);
			const item = {
				category: category,
				name: left.text().trim(),
				ingredients: utils.mapText(right.find('li')),
			};

			if(item.name === 'Dubious Food') return; // REVIEW do we actually want this one
			if(item.name === 'Rock-Hard Food') return; // REVIEW this is based on elixirs
			if(item.name === 'Fairy Tonic') return; // REVIEW this is based on elixirs
			if(_.endsWith(item.name, 'Elixir')) return; // REVIEW this is based on elixirs

			// minor inconsistencies
			if(item.name === 'Spicy Peppered Seafood') item.name = 'Spicy Pepper Seafood';
			if(item.name === 'Pepper Steak') item.name = 'Spicy Pepper Steak';
			if(item.name === 'Meat and Mushroom Skewer') item.name = 'Meat & Mushroom Skewer';
			if(item.name === 'Meat-Stuffed Pumpkins') item.name = 'Tough Meat-Stuffed Pumpkin';
			if(item.name === 'Salmon Risotto') item.name = 'Hearty Salmon Risotto';
			item.name = item.name.replace('Meuniere', 'Meunière');
			item.name = item.name.replace('Saute', 'Sauté');
			if(item.name === 'Salmon Meunière') item.name = 'Hearty Salmon Meunière';
			if(item.name === 'Sautéed Peppers') item.name = 'Spicy Sautéed Peppers';
			if(item.name === 'Honey Crepe') item.name = 'Energizing Honey Crepe';
			// wrong names
			if(_.isEqual(item, { category: 'Seafood Entrees', name: 'Clam Chowder', ingredients: ['Hearty Blueshell Snail', 'Goat Butter', 'Fresh Milk', 'Tabantha Wheat'] }))
				item.name = 'Hearty Clam Chowder';
			if(_.isEqual(item, { category: 'Desserts', name: 'Fried Bananas', ingredients: ['Mighty Bananas', 'Cane Sugar', 'Tabantha Wheat'] }))
				item.name = 'Mighty Fried Bananas';
			if(_.isEqual(item, { category: 'Desserts', name: 'Fried Bananas', ingredients: ['Mighty Bananas', 'Cane Sugar', 'Tabantha Wheat'] }))
				item.name = 'Mighty Fried Bananas';
			if(_.isEqual(item, { category: 'Desserts', name: 'Honey Candy', ingredients: ['Courser Bee Honey'] }))
				item.name = 'Energizing Honey Candy';
			if(_.isEqual(item, { category: 'Desserts', name: 'Honeyed Apple', ingredients: ['Apple', 'Courser Bee Honey'] }))
				item.name = 'Energizing Honeyed Apple';
			if(_.isEqual(item, { category: 'Vegetarian Entrees', name: 'Pumpkin Stew', ingredients: ['Fortified Pumpkin', 'Goat Butter', 'Fresh Milk', 'Tabantha Wheat'] }))
				item.name = 'Tough Pumpkin Stew';

			// in this case, between sources
			item.ingredients = item.ingredients.map((i) => {
				if(_.every(['Any', 'herb', 'flower', 'vegetable'], (s) => _.includes(i, s))) {
					return 'Any vegetable or herb';
				}
				else if(_.startsWith(i, 'Any four different ')) {
					i = i.replace('Any four different ', 'Any variety of four different ');
				}

				switch(i) {
					case 'Any other fruit': return 'Any fruit'; // REVIEW do we actually need an "any other"
					case 'Any carrot or pumpkin': return 'Any carrot or Fortified Pumpkin';
					case 'Any vegetable': return 'Any vegetable or herb';
					case 'Any four different herbs, vegetables, or flowers': return 'Any four different vegetables or herbs';
					case 'Four different fruits': return 'Any variety of four different fruits';
					case 'Hearty Blueshell Snail or any Porgy': return 'Any porgy or Hearty Blueshell Snail';
					case 'Rock salt': return 'Rock Salt';
					case 'Any Porgy': return 'Any porgy';
					case 'Raw Gourmet Meat or Raw Whole Bird': return 'Any gourmet meat';
					case 'Raw Prime Meat or Raw Bird Thigh': return 'Any prime meat';
					case 'Raw Bird Thigh or Raw Prime Meat': return 'Any prime meat';
					case 'Raw Meat or Raw Bird Drumstick': return 'Any meat';
					case 'Wildberry or Apple': return 'Any Wildberry or Apple';
					case 'Any fruit excluding apple or pumpkin': return 'Any fruit excluding Apple or Fortified Pumpkin';
					case 'Any fruit excluding apples': return 'Any fruit excluding Apple';
					case 'Any radish': return 'Any Hearty Radish or Big Hearty Radish';
					default: return i;
				}
			});

			if(_.some(item.ingredients, (i) => _.startsWith(i, 'Any'))) {
				item.generic = item.ingredients;
				delete item.ingredients;
			}

			if(item.name === 'Copious Fried Wild Greens' && _.isEqual(item.generic, ['Any vegetable or herb'])) {
				item.generic[0] = 'Any variety of four different vegetables or herbs';
			}
			else if(item.name === 'Seafood Rice Balls' && item.generic[0] === 'Any fish') {
				item.generic[0] = 'Any seafood';
			}
			// 'any meat' is to generic
			['Meat Stew', 'Meat and Rice Bowl', 'Meat and Seafood Fry', 'Salt-Grilled Meat'].forEach((name) => {
				if(item.name === name) {
					const anyIdx = item.generic.indexOf('Any meat');
					if(anyIdx !== -1)
						item.generic[anyIdx] = 'Any basic meat';
				}
			});

			utils.checkString(item, 'name', category);
			utils.checkIngredients(item, food);

			if(idx === 1) utils.verifyObject(item, checks.firstItem, 'first item in ' + category + ' does not match');
			if(idx === checks.count && item.name !== checks.lastItemName) throw new Error([
				'last item in ' + category + ' does not match',
				item.name,
				checks.lastItemName,
			].join(','));

			const found = _.find(list, { name: item.name });
			if(found) throw new Error([
				'\n',
				'already found ' + item.name,
				'  previous: ' + found.category,
				'   current: ' + category,
				JSON.stringify(found),
				JSON.stringify(item),
				'',
			].join('\n'));

			list.push(item);
		});
	}
};
