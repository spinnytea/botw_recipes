'use strict';
const _ = require('lodash');
const cheerio = require('cheerio');
const path = require('path');
const lp = require('./load_page');
const utils = require('./utils');

const ORCZ_URL = 'http://orcz.com/Breath_of_the_Wild:_Recipes';
const STRENGTHS = ['Low', 'Mid', 'High'];

exports.process = function (path_to_raw, path_to_data) {
	const database = {
		dishes: [],
	};

	return lp(path.join(path_to_raw, 'recipes.html'), ORCZ_URL).then(function (page) {
		const $ = cheerio.load(page);

		// verify page
		utils.verifyCount($('body'), 1, 'dummy check: body');
		utils.verifyCount($('.mediawiki'), 1, 'dummy check: wiki');

		// verify TOC
		// the structure of the page is very flat
		// so we can't pick out "show my h3s under this particular h2"
		// we can only check them all
		utils.verifyCount($('#content h1 .mw-headline'), 3, 'dummy check: sections');
		utils.verifyText($('#content h1 .mw-headline'), ['Cooking', 'Materials', 'Dishes']);
		utils.verifyCount($('#content h2 .mw-headline'), 6, 'dummy check: metadata (cooking/materials)');
		utils.verifyText($('#content h2 .mw-headline'), ['Details', 'Effects', 'Food', 'Critters', 'Enemy Parts', 'Fillers']);
		utils.verifyCount($('#content h3 .mw-headline'), 12, 'dummy check: dishes (for some reason these are h3)');
		utils.verifyText($('#content h3 .mw-headline'), [
			'Dishes (Restores Hearts)',
			'Dishes (Restore Stamina)',
			'Dishes (with Cold Resist)',
			'Dishes (Heat Resist)',
			'Dishes (Electric Resist)',
			'Dishes (Movement Speed)',
			'Dishes (Temporary Maximum Hearts)',
			'Dishes (Temporary Maximum Hearts)',
			'Dishes (Defense Boost)',
			'Dishes (Attack Power)',
			'Dishes (Stealth)', 'Elixirs',
		]);

		// start parsing
		parseEffectsTable($('h2:contains("Effects") + ul + table'));
		parseFoodTable($('h2:contains("Food") + table'));
		parseHeartsTable($('h3:contains("Dishes (Restores Hearts)") + table'));
		parseStaminaTable($('h3:contains("Dishes (Restore Stamina)") + p + table'));
		parseColdResistTable($('h3:contains("Dishes (with Cold Resist)") + table'));
		parseHeatResistTable($('h3:contains("Dishes (Heat Resist)") + table'));
		parseElectricResistTable($('h3:contains("Dishes (Electric Resist)") + table'));
		parseMovementSpeedTable($('h3:contains("Dishes (Movement Speed)") + table'));
		parseHeartyTable($('h3:contains("Dishes (Temporary Maximum Hearts)") + table'));
		parseDefenceTable($('h3:contains("Dishes (Defense Boost)") + table'));
		parseAttackTable($('h3:contains("Dishes (Attack Power)") + table'));
		parseStealthTable($('h3:contains("Dishes (Stealth)") + table'));

	}).then(function () {
		return lp.saveJson(database, path.join(path_to_data, 'orcz.json'));
	}).then(function () {
		return 'ORCZ Done.';
	});

	function parseEffectsTable($table) {
		database.effects = parseTable(
			$table,
			'effects',
			{
				count: 11,
				headers: ['Prefix', 'Max. Potency', 'Duration', 'Effect'],
				objects: {
					0: { name: 'Chilly', maxPotency: 2, description: 'Resistance to heat' },
				},
				lastItemName: 'Tough',
			},
			(el) => {
				const item = {
					name: utils.getText(el.find('td')[0]),
					maxPotency: +utils.getText(el.find('td')[1]),
					description: utils.getText(el.find('td')[3]),
				};
				utils.checkString(item, 'name', 'effects');
				utils.checkNumber(item, 'maxPotency', 2, 25, 'effects');
				utils.checkString(item, 'description', 'effects');
				return item;
			}
		);
	}

	function parseFoodTable($table) {
		const effects = _.map(database.effects, 'name');
		database.food = parseTable(
			$table,
			'food',
			{
				count: 85,
				headers: ['Name', 'Type', 'Hearts', 'Effect', 'Potency', 'Duration', 'Resale'],
				objects: {
					0: { name: 'Acorn', hearts: 0.5, effect: '-', potency: '-', duration: '0:50' },
				},
				lastItemName: 'Shard of Farosh\'s Horn',
			},
			(el) => {
				const item = {
					name: utils.getText(el.find('td')[0]),
					hearts: parseFloat(utils.getText(el.find('td')[2])) * 2,
					effect: utils.getText(el.find('td')[3]),
					potency: utils.getText(el.find('td')[4]),
					duration: utils.getText(el.find('td')[5]),
				};

				// minor inconsitencies
				if(item.name === 'Monster Extract' && item.effect === '') item.effect = '-';
				if(item.name === 'Cool Saffina') item.name = 'Cool Safflina';
				if(item.name === 'Warm Saffina') item.name = 'Warm Safflina';
				if(item.name === 'Electric Saffina') item.name = 'Electric Safflina';
				if(item.name === 'Bright-eyed Crab') item.name = 'Bright-Eyed Crab';
				if(item.name === 'Monster Extract') item.hearts = 0;
				if(item.name === 'Star Fragment') item.hearts = 0;
				if(item.name === 'Fairy') item.hearts = 5;

				utils.checkString(item, 'name', 'food');
				utils.checkNumber(item, 'hearts', 0, 8, 'food');
				utils.checkString(item, 'effect', 'food');
				if(item.effect !== '-') utils.checkInArray(item, 'effect', effects, 'food');
				utils.checkString(item, 'potency', 'food');
				utils.checkString(item, 'duration', 'food');
				return item;
			}
		);
	}

	function parseHeartsTable($table) {
		const list = parseTable(
			$table,
			'dishes_hearts',
			{
				count: 72,
				headers: ['Food', 'Ingredients', 'Hearts Restored', 'Notes', 'Sell Price'],
				objects: {
					0: { name: 'Apple Pie', ingredients: ['Apple', 'Tabantha Wheat', 'Cane Sugar', 'Goat Butter'], hearts: 3, effect: null, notes: 'Tabantha Wheat, Cane Sugar & Goat Butter obtainable in Rito Village.' },
					58: { name: 'Hard-Boiled Egg', ingredients: ['Bird Egg'], hearts: 1.5, effect: null, notes: 'Drop into hot springs.' },
				},
				lastItemName: 'Toasty Hylian Shroom',
			},
			(el) => {
				const item = {
					name: utils.getText(el.find('td')[0]),
					ingredients: utils.getTextList(el.find('td')[1]),
					hearts: utils.getText(el.find('td')[2]),
					notes: utils.getText(el.find('td')[3]),
				};
				item.effect = handleEffect(item.name);

				// minor inconsitencies
				if(item.name === 'Meat Skewer') item.hearts = '0';
				if(item.name === 'Hard Boiled Egg') item.name = 'Hard-Boiled Egg';
				if(item.name === 'Fruit cake') item.name = 'Fruitcake';
				if(item.name === 'Mushroom risotto') item.name = 'Mushroom Risotto';
				if(item.name === 'Herb Saute') item.name = 'Herb Sauté';
				if(item.name === 'Fragrant Mushroom Saute') item.name = 'Fragrant Mushroom Sauté';
				if(item.name === 'Milk') item.name = 'Warm Milk';
				if(item.name === 'Meat and Mushroom Skewer') item.name = 'Meat & Mushroom Skewer';
				item.hearts = handleFractions(item.hearts);

				utils.checkString(item, 'name', 'dishes_hearts');
				if(item.effect !== null) throw new Error('base recipes should not have an effect');
				handleIngredients(item, 'dishes_hearts');
				utils.checkNumber(item, 'hearts', 0, 20, 'dishes_hearts');
				utils.checkString(item, 'notes', 'dishes_hearts');
				return item;
			}
		);
		Array.prototype.push.apply(database.dishes, list);
	}

	function parseStaminaTable($table) {
		const list = parseTable(
			$table,
			'dishes_stamina',
			{
				count: 25,
				headers: ['Food', 'Ingredients', 'Effect', 'Notes', 'Sell Price'],
				objects: {
					0: { name: 'Enduring Fried Wild Greens', effect: 'Enduring', ingredients: ['Endura Carrot'], notes: 'Restore four hearts and overfill your stamina wheel. (5 Endura Carrots overfills for 2 stamina wheels.)' },
					3: { name: 'Energizing Seafood Skewer', effect: 'Energizing', ingredients: ['Bright-Eyed Crab', 'Bright-Eyed Crab', 'Bright-Eyed Crab', 'Bright-Eyed Crab', 'Bright-Eyed Crab'], notes: 'Restore five hearts and three stamina wheels (full).' },
				},
				lastItemName: 'Energizing Nutcake',
			},
			(el) => {
				const item = {
					name: utils.getText(el.find('td')[0]),
					ingredients: utils.getTextList(el.find('td')[1]),
					notes: utils.getText(el.find('td')[2]) +
						(() => { const more = utils.getText(el.find('td')[3]); if(more) return ' (' + more + ')'; else return ''; })(),
				};
				item.effect = handleEffect(item.name);

				// minor inconsitencies
				if(item.name === 'Energizing Fish Skewer' && item.ingredients[0] === 'Fresh Fish') item.ingredients[0] = 'Staminoka Bass';

				utils.checkString(item, 'name', 'dishes_stamina');
				utils.checkInArray(item, 'effect', ['Energizing', 'Enduring'], 'dishes_stamina');
				handleIngredients(item, 'dishes_stamina');
				utils.checkString(item, 'notes', 'dishes_stamina');
				return item;
			}
		);
		Array.prototype.push.apply(database.dishes, list);
	}

	function parseColdResistTable($table) {
		const list = parseTable(
			$table,
			'dishes_cold_resist',
			{
				count: 21,
				headers: ['Food', 'Ingredients', 'Strength', 'Duration', 'Notes', 'Sell Price'],
				objects: {
					0: { name: 'Spicy Steamed Mushrooms', effect: 'Spicy', ingredients: ['Sunshroom', 'Sunshroom', 'Sunshroom', 'Warm Safflina', 'Shard of Farosh\'s Horn'], strength: 'Mid', notes: 'Restores 9 hearts' },
				},
				lastItemName: 'Spicy Fruit and Mushroom Mix',
			},
			(el) => {
				const item = {
					name: utils.getText(el.find('td')[0]),
					ingredients: utils.getTextList(el.find('td')[1]),
					strength: utils.getText(el.find('td')[2]),
					notes: utils.getText(el.find('td')[4]),
				};
				item.effect = handleEffect(item.name);

				// minor inconsitencies
				if(item.name === 'Spicy Salt-Grilled Gourmet Meat' && item.strength === '') item.strength = 'Low'; // HACK test for myself
				if(item.name === 'Spicy Sauteed Peppers') item.name = 'Spicy Sautéed Peppers';
				if(item.name === 'Spicy Meat and Mushroom Skewer') item.name = 'Spicy Meat & Mushroom Skewer';
				if(item.strength === 'Medium') item.strength = 'Mid';

				utils.checkString(item, 'name', 'dishes_cold_resist');
				utils.checkInArray(item, 'effect', ['Spicy'], 'dishes_cold_resist');
				handleIngredients(item, 'dishes_cold_resist');
				utils.checkInArray(item, 'strength', STRENGTHS, 'dishes_cold_resist');
				utils.checkString(item, 'notes', 'dishes_cold_resist');
				return item;
			}
		);
		Array.prototype.push.apply(database.dishes, list);
	}

	function parseHeatResistTable($table) {
		const list = parseTable(
			$table,
			'dishes_heat_resist',
			{
				count: 10,
				headers: ['Food', 'Ingredients', 'Hearts', 'Strength', 'Duration', 'Notes', 'Sell Price'],
				objects: {
					0: { name: 'Chilly Fruit and Mushroom Mix', effect: 'Chilly', ingredients: ['Chillshroom', 'Chillshroom', 'Chillshroom', 'Hydromelon', 'Shard of Farosh\'s Horn'], strength: 'Mid', notes: 'Restores 7 hearts' },
					7: { name: 'Chilly Simmered Fruit', effect: 'Chilly', ingredients: ['Hydromelon', 'Hydromelon', 'Hydromelon', 'Hydromelon', 'Monster Extract'], strength: 'Mid', notes: 'recipe sometimes makes low level for 30 min., or mid level for 10 min, also restores a few hearts' },
				},
				lastItemName: 'Chilly Spiced Meat Skewer',
			},
			(el) => {
				const item = {
					name: utils.getText(el.find('td')[0]),
					ingredients: utils.getTextList(el.find('td')[1]),
					strength: utils.getText(el.find('td')[3]),
					notes: (utils.getText(el.find('td')[2]) + ' ' + utils.getText(el.find('td')[5])).trim(),
				};
				item.effect = handleEffect(item.name);

				// minor inconsitencies
				if(item.name === 'Chilly Simmered Fruit' && item.strength === 'Low-Mid') item.strength = 'Mid';
				if(item.name === 'Chilly Spiced Meat Skewer' && item.strength === '4:30') {
					item.strength = 'Low';
					item.notes = utils.getText(el.find('td')[4]);
					if(item.notes !== 'you can use any type of meat to upgrade it') throw new Error('formatting fixed?');
				}
				if(item.name === 'Chilly Elixir') return;
				if(item.notes === '') item.notes = '-'; // TODO write it myself
				item.notes = item.notes.replace('see notes ', '');

				utils.checkString(item, 'name', 'dishes_heat_resist');
				utils.checkInArray(item, 'effect', ['Chilly'], 'dishes_heat_resist');
				handleIngredients(item, 'dishes_heat_resist');
				utils.checkInArray(item, 'strength', STRENGTHS, 'dishes_heat_resist');
				utils.checkString(item, 'notes', 'dishes_heat_resist');
				return item;
			}
		);
		Array.prototype.push.apply(database.dishes, list);
	}

	function parseElectricResistTable($table) {
		const list = parseTable(
			$table,
			'dishes_electric_resist',
			{
				count: 26,
				headers: ['Food', 'Ingredients', 'Strength', 'Duration', 'Notes', 'Sell Price'],
				objects: {
					0: { name: 'Electro Fruit and Mushroom Mix', effect: 'Electro', ingredients: ['Voltfruit', 'Voltfruit', 'Voltfruit', 'Zapshroom', 'Shard of Farosh\'s Horn'], strength: 'High', notes: 'Restores 7 hearts' },
				},
				lastItemName: 'Electro Mushroom Skewer',
			},
			(el) => {
				const item = {
					name: utils.getText(el.find('td')[0]),
					ingredients: utils.getTextList(el.find('td')[1]),
					strength: utils.getText(el.find('td')[2]),
					notes: utils.getText(el.find('td')[4]),
				};
				item.effect = handleEffect(item.name);

				utils.checkString(item, 'name', 'dishes_electric_resist');
				utils.checkInArray(item, 'effect', ['Electro'], 'dishes_electric_resist');
				handleIngredients(item, 'dishes_electric_resist');
				utils.checkInArray(item, 'strength', STRENGTHS, 'dishes_electric_resist');
				utils.checkString(item, 'notes', 'dishes_electric_resist');
				return item;
			}
		);
		Array.prototype.push.apply(database.dishes, list);
	}

	function parseMovementSpeedTable($table) {
		const list = parseTable(
			$table,
			'dishes_hasty',
			{
				count: 18,
				headers: ['Food', 'Ingredients', 'Strength', 'Duration', 'Notes', 'Sell Price'],
				objects: {
					0: { name: 'Hasty Meat Skewer', effect: 'Hasty', ingredients: ['Fleet-Lotus Seeds', 'Fleet-Lotus Seeds', 'Fleet-Lotus Seeds', 'Fleet-Lotus Seeds', 'Raw Meat'], strength: 'High', notes: 'Restores 6 hearts' },
				},
				lastItemName: 'Hasty Simmered Fruit',
			},
			(el) => {
				const item = {
					name: utils.getText(el.find('td')[0]),
					ingredients: utils.getTextList(el.find('td')[1]),
					strength: utils.getText(el.find('td')[2]),
					notes: utils.getText(el.find('td')[4]),
				};
				item.effect = handleEffect(item.name);

				// minor inconsitencies
				if(item.name === 'Hasty Simmered Fruit' && item.strength === 'Low to High') item.strength = 'High';

				utils.checkString(item, 'name', 'dishes_hasty');
				utils.checkInArray(item, 'effect', ['Hasty'], 'dishes_hasty');
				handleIngredients(item, 'dishes_hasty');
				utils.checkInArray(item, 'strength', STRENGTHS, 'dishes_hasty');
				utils.checkString(item, 'notes', 'dishes_hasty');
				return item;
			}
		);
		Array.prototype.push.apply(database.dishes, list);
	}

	function parseHeartyTable($table) {
		const list = parseTable(
			$table,
			'dishes_hearty',
			{
				count: 38,
				headers: ['Food', 'Ingredients', 'Effect', 'Notes', 'Sell Price'],
				objects: {
					0: { name: 'Hearty Fruitcake', effect: 'Hearty', ingredients: ['Wildberry', 'Hearty Durian', 'Hearty Durian', 'Tabantha Wheat', 'Cane Sugar'],
						notes: 'Fully restore hearts +8 temporary maximum hearts This recipe requires two different fruit, cane sugar and tabantha wheat(Apple,Wildberry,Palm Fruit and Hearty Durian only) other fruit will result in a different dish like Sneaky Mighty Tough or plain Fruitcake etc.' },
				},
				lastItemName: 'Hearty Fish Skewer',
			},
			(el) => {
				const item = {
					name: utils.getText(el.find('td')[0]),
					ingredients: utils.getTextList(el.find('td')[1]),
					notes: (utils.getText(el.find('td')[2]) + ' ' + utils.getText(el.find('td')[3])).trim(),
				};
				item.effect = handleEffect(item.name);

				// minor inconsitencies
				if(item.name === 'Hasty Simmered Fruit' && item.strength === 'Low to High') item.strength = 'High';
				if(item.name === 'Hearty Salmon Meuniére') item.name = 'Hearty Salmon Meunière';
				if(item.name === 'Hearty Meat and Mushroom Skewer') item.name = 'Hearty Meat & Mushroom Skewer';
				if(item.name === 'Hearty Seafood Rice Balls' && _.isEqual(item.ingredients, ['Fresh Milk', 'Tabantha Wheat', 'Goat Butter', 'Hearty Blueshell Snail'])) {
					item.name = 'Hearty Clam Chowder';
					item.notes = item.notes.replace('This happened when using the Hearty Clam Chowder recipe. ', '');
				}
				if(item.name === 'Hearty Steamed Fish' && _.isEqual(item.ingredients, ['Hearty Blueshell Snail', 'Big Hearty Radish', 'Hearty Truffle'])) {
					item.name = 'Hearty Steamed Mushrooms';
				}
				if(item.name === 'Hearty Steamed Fish' && _.isEqual(item.ingredients, ['Hearty Durian x2', 'Big Hearty Radish', 'Hearty Blueshell Snail x2'])) {
					item.name = 'Hearty Steamed Fruit';
				}
				if(item.name === 'Hearty Mushroom Skewer' && _.isEqual(item.ingredients, ['Hearty Durian', 'Hearty Truffle', 'Big Hearty Truffle'])) {
					item.name = 'Hearty Fruit and Mushroom Mix';
				}

				utils.checkString(item, 'name', 'dishes_hearty');
				utils.checkInArray(item, 'effect', ['Hearty'], 'dishes_hearty');
				handleIngredients(item, 'dishes_hearty');
				utils.checkString(item, 'notes', 'dishes_hearty');
				return item;
			}
		);
		Array.prototype.push.apply(database.dishes, list);
	}

	function parseDefenceTable($table) {
		const list = parseTable(
			$table,
			'dishes_defence',
			{
				count: 31,
				headers: ['Food', 'Ingredients', 'Strength', 'Duration', 'Notes', 'Sell Price'],
				objects: {
					0: { name: 'Tough Salt-Grilled Fish', effect: 'Tough', ingredients: ['Ironshroom', 'Ironshroom', 'Armored Carp', 'Armored Carp', 'Rock Salt'], strength: 'High', notes: 'Restores five hearts' },
				},
				lastItemName: 'Tough Salt-Grilled Fish',
			},
			(el) => {
				const item = {
					name: utils.getText(el.find('td')[0]),
					ingredients: utils.getTextList(el.find('td')[1]),
					strength: utils.getText(el.find('td')[2]),
					notes: utils.getText(el.find('td')[4]),
				};
				item.effect = handleEffect(item.name);

				// minor inconsitencies
				if(item.name === 'Tough Salt-Grilled ~Fish') item.name = 'Tough Salt-Grilled Fish'; // TODO apparently, I need to make a styleguide for the names
				if(item.name === 'Tough Meat and Mushroom Skewer') item.name = 'Tough Meat & Mushroom Skewer';
				if(item.name === 'Tough Meat & Mushroom Skewer' && item.notes === '') item.notes = '-'; // TODO write it myself

				utils.checkString(item, 'name', 'dishes_defence');
				utils.checkInArray(item, 'effect', ['Tough'], 'dishes_defence');
				handleIngredients(item, 'dishes_defence');
				utils.checkInArray(item, 'strength', STRENGTHS, 'dishes_defence');
				utils.checkString(item, 'notes', 'dishes_defence');
				return item;
			}
		);
		Array.prototype.push.apply(database.dishes, list);
	}

	function parseAttackTable($table) {
		const list = parseTable(
			$table,
			'dishes_attack',
			{
				count: 23,
				headers: ['Food', 'Ingredients', 'Strength', 'Duration', 'Notes', 'Sell Price'],
				objects: {
					0: { name: 'Mighty Crab Risotto', effect: 'Mighty', ingredients: ['Hylian Rice', 'Goat Butter', 'Rock Salt', 'Razorclaw Crab'], strength: 'Low', notes: 'Restore four hearts' },
				},
				lastItemName: 'Mighty Steamed Fruit',
			},
			(el) => {
				const item = {
					name: utils.getText(el.find('td')[0]),
					ingredients: utils.getTextList(el.find('td')[1]),
					strength: utils.getText(el.find('td')[2]),
					notes: utils.getText(el.find('td')[4]),
				};
				item.effect = handleEffect(item.name);

				// minor inconsistencies
				if(item.name === 'Mighty Herb Sautè') item.name = 'Mighty Herb Sauté';
				if(item.name === 'Mighty Steamed Fish' && _.isEqual(item.ingredients, ['Bird Egg', 'Hyrule Herb', 'Razorclaw Crab'])) {
					item.ingredients[2] = 'Mighty Porgy';
					item.notes = item.notes.replace(' - Razorclaw Crab can be swapped out for Mighty Porgy', '');
				}

				utils.checkString(item, 'name', 'dishes_attack');
				utils.checkInArray(item, 'effect', ['Mighty'], 'dishes_attack');
				handleIngredients(item, 'dishes_attack');
				utils.checkInArray(item, 'strength', STRENGTHS, 'dishes_attack');
				utils.checkString(item, 'notes', 'dishes_attack');
				return item;
			}
		);
		Array.prototype.push.apply(database.dishes, list);
	}

	function parseStealthTable($table) {
		const list = parseTable(
			$table,
			'dishes_stealth',
			{
				count: 23,
				headers: ['Food', 'Ingredients', 'Strength', 'Duration', 'Notes', 'Sell Price'],
				objects: {
					0: { name: 'Sneaky Fried Wild Greens', effect: 'Sneaky', ingredients: ['Silent Princess', 'Silent Princess', 'Silent Princess', 'Fairy', 'Shard of Farosh\'s Horn'], strength: 'High', notes: 'Restore 22 3/4 hearts' },
					20: { name: 'Sneaky Mushroom Skewer', effect: 'Sneaky', ingredients: ['Silent Shroom'], strength: 'Low', notes: 'Restore one heart' },
				},
				lastItemName: 'Sneaky Fried Wild Greens',
			},
			(el) => {
				const item = {
					name: utils.getText(el.find('td')[0]),
					ingredients: utils.getTextList(el.find('td')[1]),
					strength: utils.getText(el.find('td')[2]),
					notes: utils.getText(el.find('td')[4]),
				};
				item.effect = handleEffect(item.name);

				// minor inconsitencies
				if(item.strength === 'High-Level Steath Effect') item.strength = 'High';
				if(item.name === 'Sneaky Fried Wild Greens' && item.notes === '') item.notes = '-'; // TODO write it myself
				if(item.name === 'Sneaky Seafood Meuniere') item.name = 'Sneaky Seafood Meunière';
				if(item.name === 'Sneaky Fish and Mushroom Skewer' && _.isEqual(item.ingredients, ['Silent Shroom x4', 'Sneaky River Snail 1x'])) {
					item.name = 'Sneaky Seafood Skewer';
				}
				if(item.name === 'Sneaky Steamed Fish' && _.isEqual(item.ingredients, ['Silent Princess', 'Silent Shroom x2', 'Sneaky River Snail x2'])) {
					item.name = 'Sneaky Steamed Mushrooms';
				}
				if(item.name === 'Sneaky Steamed Fish' && _.isEqual(item.ingredients, ['Silent Shroom', 'Blue Nightshade', 'Sneaky River Snail'])) {
					item.name = 'Sneaky Steamed Mushrooms';
				}

				utils.checkString(item, 'name', 'dishes_stealth');
				utils.checkInArray(item, 'effect', ['Sneaky'], 'dishes_stealth');
				handleIngredients(item, 'dishes_stealth');
				utils.checkInArray(item, 'strength', STRENGTHS, 'dishes_stealth');
				utils.checkString(item, 'notes', 'dishes_stealth');
				return item;
			}
		);
		Array.prototype.push.apply(database.dishes, list);
	}

	function parseTable($table, category, checks, parseRowFn) {
		utils.verifyCount($table, 1, 'one ' + category + ' table should be found');
		const $rows = $table.find('tr');
		utils.verifyCount($rows, checks.count + 1, category + ': table rows in ' + category);

		const list = [];
		$rows.each((idx, el) => {
			el = cheerio(el);
			if(idx === 0) {
				utils.verifyText(el.find('th'), checks.headers);
			}
			else {
				const obj = parseRowFn(el);
				if(obj) list.push(obj);
			}
		});

		// spot check some values
		if(_.isEmpty(checks.objects)) throw new Error('need to check at least one parsed row in ' + category);
		_.forEach(checks.objects, (obj, idx) => utils.verifyObject(list[idx], obj, 'item in ' + category + ' does not match'));
		utils.verifyString(_.last(list).name, checks.lastItemName, 'last item in ' + category + ' does not match');

		return list;
	}

	//

	function handleFractions(str) {
		// minor inconsitencies (well, the 1/4 part should be the single character)
		[['¼', '.25'], ['1/4', '.25'], ['½', '.5'], ['¾', '.75']].forEach(([fraction, decimal]) => {
			let after = str.replace(fraction, decimal);
			if(str !== after) {
				after = after.replace(/\s+/g, '');
				// console.log(str, '=>', after);
			}
			str = after;
		});
		if(str[0] === '.') str = '0' + str;

		const number = parseFloat(str, 10);

		// check that all fractions have been handled
		if(str !== number.toString())
			throw new Error('need to handle ' + str + ' ('+number+', '+number.toString()+')');

		return number;
	}

	function handleIngredients(item, category) {
		utils.checkString(item, 'ingredients', category);
		const list = item.ingredients;
		if(_.isEmpty(list)) throw new Error('there has to be at least one ingredient');

		// expand items (x2, x3, etc)
		const copy = list.slice(0).reverse();
		copy.forEach((i, idx) => {
			idx = copy.length-idx-1;
			if(_.endsWith(i, '1x') || _.endsWith(i, 'x1')) {
				i = i.substr(0, i.length-3);
				list.splice(idx, 1, i);
			}
			if(_.endsWith(i, 'x2')) {
				i = i.substr(0, i.length-3);
				list.splice(idx, 1, i, i);
			}
			if(_.endsWith(i, 'x 2')) {
				i = i.substr(0, i.length-4);
				list.splice(idx, 1, i, i);
			}
			if(_.endsWith(i, 'x3')) {
				i = i.substr(0, i.length-3);
				list.splice(idx, 1, i, i, i);
			}
			if(_.endsWith(i, 'x4')) {
				i = i.substr(0, i.length-3);
				list.splice(idx, 1, i, i, i, i);
			}
			if(_.endsWith(i, 'x5')) {
				i = i.substr(0, i.length-3);
				list.splice(idx, 1, i, i, i, i, i);
			}
			if(i === 'Any') {
				// just remove it
				list.splice(idx, 1);
			}
		});

		// check if found
		list.forEach((i, idx) => {
			// minor inconsitencies
			// they thread started copypastaing the wrong name
			i = i.replace('[Dragon]', 'Farosh');
			if(i === 'Hyrule Shroom') i = 'Hylian Shroom';
			if(i === 'Cool Saffina') i = 'Cool Safflina';
			if(i === 'Electric Saffina') i = 'Electric Safflina';
			if(i === 'Hyrule Herbs') i = 'Hyrule Herb';
			if(i === 'Egg') i = 'Bird Egg';
			if(i === 'Enduring Carrot') i = 'Endura Carrot';
			if(i === 'Spicy Peppers') i = 'Spicy Pepper';
			if(i === 'Gourmet Meat') i = 'Raw Gourmet Meat';
			if(i === 'Farosh\'s Horn Shard') i = 'Shard of Farosh\'s Horn';
			if(i === 'Milk') i = 'Fresh Milk';
			if(i === 'Fleet Lotus Seed') i = 'Fleet-Lotus Seeds';

			const f = _.find(database.food, (_f) => { return _f.name.toLowerCase() === i.toLowerCase(); });
			if(!f) throw new Error(i + ' not found in a valid ingredient; ' + JSON.stringify(list));
			list[idx] = f.name;
		});

		// since we are trying to make a standard
		utils.checkIngredients(item, database.food);
	}

	function handleEffect(name) {
		const effect = database.effects.find((e) => _.startsWith(name, e.name));
		if(effect) return effect.name;
		return null;
	}
};