'use strict';
const _ = require('lodash');
const cheerio = require('cheerio');
const path = require('path');
const lp = require('./load_page');
const utils = require('./utils');

// the wiki category is split across 2 pages (page sizes of 200)
// we should be able to detect if page 2 is wrong url because the counts won't line up
const PAGE_1 = 'https://zelda.fandom.com/wiki/Category:Dishes';
const PAGE_2 = 'https://zelda.fandom.com/wiki/Category:Dishes?from=Toasty+Hylian+Shroom';
const URL_ROOT = 'https://zelda.fandom.com'; // for building links when they aren't absolute paths

// throttle the requests (since they are cached, we can just increase and run again)
const START_WITH = 0;
const MAX_COUNT = 300;

exports.SKIPPED_NAMES = [
	'Dubious Food', // REVIEW maybe Dubious Food should still be an option

	// these are nice-to-haves, see if they are in other lists
	// we can check them in other lists
	'Electro Steamed Meat', // I think this one is a mistake
	'Energizing Salt-Grilled Fish', // I think this one is a mistake
	'Gourmet Meat and Rice Bowl', // it says "Found: Cooking ingredients"
	'Gourmet Meat and Seafood Fry', // it says "Found: Cooking ingredients"
	'Gourmet Poultry Pilaf', // it says "Found: Cooking ingredients"
	'Hasty Mushroom Skewer', // there is no 'Found' section, so no recipe
	'Hearty Fried Wild Greens', // not a good description, and we have it elsewhere
	'Hearty Salt-Grilled Fish', // not a good description, and we have it elsewhere
	'Honey Crepe', // it says "Found: Cooking"
	'Pepper Seafood', // it says "Found: Cooking"
	'Plain Crepe', // it says "Found: Cooking ingredients"
	'Poultry Pilaf', // it says "Found: Cooking ingredients"
	'Prime Meat and Rice Bowl', // it says "Found: Cooking ingredients"
	'Prime Meat and Seafood Fry', // it says "Found: Cooking ingredients"
	'Seafood Meunière', // it says "Found: Cooking ingredients"
	'Seafood Skewer', // it says "Found: Cooking ingredients"
	'Simmered Fruit', // I think this one is a mistake; or if not, there's lots of conflicts on other sources
	'Sneaky Mushroom Skewer', // there is no 'Found' section, so no recipe
	'Sneaky Steamed Meat', // "Cooking meat and fragrant leaves"
	'Sneaky Steamed Mushrooms', // "Cooking mushrooms and plant leaves"
	'Tough Meat & Mushroom Skewer', // it says "Found: Cooking"
	'Wildberry Crepe', // it says "Found: Cooking"
];

exports.process = function (path_to_raw, path_to_data) {
	const list = [];
	let food = [];

	// load some other data up front
	return lp.loadJson(path.join(path_to_data, 'orcz.json')).then(function (database) {
		food = database.food;
	}).then(function () {
		// load both pages of the categories
		return lp(path.join(path_to_raw, 'dishes1.html'), PAGE_1).then(function (page1) {
			return lp(path.join(path_to_raw, 'dishes2.html'), PAGE_2).then(function (page2) {
				return [page1, page2];
			});
		});
	}).then(function ([page1, page2]) {
		const $1 = cheerio.load(page1);
		const $2 = cheerio.load(page2);

		// verify page
		utils.verifyCount($1('body'), 1, 'dummy check: body');
		utils.verifyCount($1('.mediawiki'), 1, 'dummy check: wiki');
		utils.verifyString(utils.getText($1('p.category-page__total-number')[0]), 'All items (219)', 'total count on page');
		utils.verifyCount($2('body'), 1, 'dummy check: body');
		utils.verifyCount($2('.mediawiki'), 1, 'dummy check: wiki');
		utils.verifyString(utils.getText($2('p.category-page__total-number')[0]), 'All items (219)', 'total count on page');

		utils.verifyCount($1('li.category-page__member'), 200, 'dummy check: wiki');
		utils.verifyCount($2('li.category-page__member'), 19, 'dummy check: wiki');

		// start parsing
		let count = 0;
		let next = Promise.resolve();
		[$1, $2].forEach(($) => {
			$('li.category-page__member').each((idx, el) => {
				count++;
				if(START_WITH > count || count > MAX_COUNT) return;

				el = cheerio(el);
				next = next.then(() => parseItem(el)).then((item) => {
					if(item) list.push(item);
				});
			});
		});

		return next;
	}).then(function () {
		// spot check some items
		[
			{ name: 'Baked Apple', method: 'Roasting', ingredients: ['Apple'] },
			{ name: 'Blackened Crab', method: 'Roasting', generic: ['Any crab'] },
			{ name: 'Crab Stir-Fry', method: 'Cooking', generic: ['Any crab', 'Goron Spice'] },
			{ name: 'Charred Pepper', method: 'Roasting', ingredients: ['Spicy Pepper'] },
			{ name: 'Enduring Mushroom Skewer', method: 'Cooking', generic: ['Any meat', 'Endura Shroom'] },
			{ name: 'Fruit Pie', method: 'Cooking', generic: ['Any fruit excluding Apple or Fortified Pumpkin', 'Cane Sugar', 'Goat Butter', 'Tabantha Wheat'] },
			{ name: 'Meat and Seafood Fry', method: 'Cooking', generic: ['Any basic meat', 'Any seafood'] },
		].forEach((item) => {
			const actual = _.omit(_.find(list, { name: item.name }), ['localImage', 'image', 'link']);
			if(_.isEmpty(actual)) {
				console.log(item.name + ' was not found in the list');
			}
			else {
				utils.verifyObject(actual, item, 'item ' + item.name + ' does not match');
			}
		});
	}).then(function () {
		return lp.saveJson(list, path.join(path_to_data, 'fandom_dishes.json'));
	}).then(function () {
		return 'Fandom Dishes Done.';
	});

	function parseItem(el) {
		utils.verifyCount(el.find('>a'), 1, 'only 1 link per item');

		const item = {
			name: utils.getText(el.find('>a')),
			link: URL_ROOT + el.find('>a').attr('href'),
		};

		// minor inconsistencies
		item.name = item.name.replace('Meat and Mushroom Skewer', 'Meat & Mushroom Skewer');

		// skip these items
		// no elixirs (not interested in those right now)
		if(_.endsWith(item.name, 'Elixir')) {
			return Promise.resolve(null);
		}
		// lost of issues with this list
		if(_.includes([
			'Akkala Buns', // not something we can make; it also doesn't have a picture
			'Failed Experiment', // in beta versions
			'Grilled Rock Roast', // this is a special thing that you carry, cannot eat
			'Hearty Wild Greens', // should actually be Hearty Fried Wild Greens
			'Milk', // ... wrong game?
			'Steak Skewer', // should actually be Meat Skewer
			'Fairy Tonic', // this is based on elixirs
			'Rock-Hard Food', // similar to dubious food, except, this is targeting elixirs, which I don't do right now
		], item.name)) {
			return Promise.resolve(null);
		}

		return lp(path.join(path_to_raw, '/dishes_pages/'+item.name+'.html'), item.link).then(function (page) {
			const $ = cheerio.load(page);
			const $info = $('aside.portable-infobox');

			utils.verifyString(utils.getText($('.page-header__title')).replace('&', 'and'), item.name.replace('&', 'and'), 'page name should match item name');
			utils.verifyCount($info, 1, 'dummy check: page info box');
			utils.verifyString(utils.getText($info.find('h2')).replace('&', 'and'), item.name.replace('&', 'and'), 'info name should match item name');

			// we can't get much info from these
			// but their icon is still useful
			if(!_.includes(exports.SKIPPED_NAMES, item.name)) {
				handleFound(item, utils.getTextList($info.find('.pi-data-label:contains("Found")+.pi-data-value')[0]));
			}

			item.image = $info.find('.pi-image img').attr('src');
			utils.checkString(item, 'image', item.name);
			if(!_.startsWith(item.image, 'https://vignette.wikia.nocookie.net/zelda/images/')) throw new Error('image src looks wrong, perfix, ' + item.name);
			if(!_.includes(item.image, '.png')) throw new Error('image src looks wrong, type, ' + item.name);

			// load the image, and then move on
			// item.localImage = path.join('images', 'dishes', item.name+'.png');
			// return lp.download(path.join('botw_recipes_vue/public', item.localImage), item.image, null).then(() => {
			// 	return item;
			// });

			return item;
		});
	}

	function handleFound(item, found_raw) {
		utils.checkString({ found_raw }, 'found_raw', item.name);

		// we can just ignore filler words
		_.pullAll(found_raw, [':', '-', 'and', 'with', 'a']);
		// we can just ignore generic locations / events
		_.pullAll(found_raw, ['Rescued travelers']);
		found_raw = found_raw.map((line) => _.trim(line, ':- '));

		utils.replaceList(found_raw, ['Raw Bird Drumstick', 'or', 'Raw Meat'], 'Any meat');
		utils.replaceList(found_raw, ['Raw Meat', 'or', 'Raw Bird Drumstick'], 'Any meat');
		utils.replaceList(found_raw, ['Raw Bird Thigh', 'or', 'Raw Prime Meat'], 'Any prime meat');
		utils.replaceList(found_raw, ['Raw Gourmet Meat', 'or', 'Raw Whole Bird'], 'Any gourmet meat');
		utils.replaceList(found_raw, ['any', 'fish', 'crab', 'or snail'], 'Any seafood');
		utils.replaceList(found_raw, ['any', 'fish', 'snail', 'or', 'crab'], 'Any seafood');
		utils.replaceList(found_raw, ['any fish', 'crab', 'or snail'], 'Any seafood');

		// minor inconsitencies
		if(item.name === 'Energizing Glazed Meat' && found_raw.length === 2) {
			found_raw.push('Cooking');
		}
		if(item.name === 'Nutcake' && found_raw[0] === 'Ingredients') {
			found_raw[0] = 'Cooking';
		}
		if(item.name === 'Charred Pepper' && found_raw[0] === 'Cooking') {
			found_raw[0] = 'Roasting';
		}
		// 'any meat' is to generic
		['Spiced Meat Skewer', 'Meat Stew', 'Meat and Seafood Fry', 'Salt-Grilled Meat'].forEach((name) => {
			if(item.name === name) {
				const idx = found_raw.indexOf('Any meat');
				if(idx !== -1)
					found_raw[idx] = 'Any basic meat';
			}
		});

		if(item.name in HARD_CODED) {
			_.assign(item, HARD_CODED[item.name]);
			found_raw.splice(0); // handled manually
		}

		else if(_.includes(found_raw, 'Cooking') || _.includes(found_raw, 'Roasting')) {
			if(_.includes(found_raw, 'Cooking')) {
				_.pull(found_raw, 'Cooking');
				item.method = 'Cooking';
			}
			else if(_.includes(found_raw, 'Roasting')) {
				_.pull(found_raw, 'Roasting');
				item.method = 'Roasting';
			}
			let ingredients = [];
			let isGeneric = false;
			found_raw.slice(0).forEach((line) => {
				let name = _.trim(line, ' -:');
				// minor inconsitencies
				if(name === 'Spicy Peppers') name = 'Spicy Pepper';
				if(name === 'Apples') name = 'Apple';
				if(name === 'Stamella Shrooms') name = 'Stamella Shroom';
				if(name === 'Hylian Shrooms') name = 'Hylian Shroom';
				if(name === 'Hearty Truffles') name = 'Hearty Truffle';
				if(name === 'Crab') name = 'Any crab'; // Any / Crab was broken up over two lines
				if(name === 'fish') name = 'Any fish'; // Any / fish was broken up over two lines
				if(name === 'Fruit') name = 'Any fruit'; // Any type of / Fruit was broken up over two lines
				if(name.toLowerCase() === 'any mushrooms') name = 'Any mushroom';
				if(name.toLowerCase() === 'any herb or vegetable') name = 'Any vegetable or herb';
				if(name.toLowerCase() === 'any herbs or vegetables') name = 'Any vegetable or herb';
				if(name.toLowerCase() === 'any herbs or vegetables') name = 'Any vegetable or herb';
				if(name.toLowerCase() === 'any four different vegetables or herbs') name = 'Any variety of four different vegetables or herbs';
				if(name.toLowerCase() === 'any variety of four or five different fruits') name = 'Any variety of four different fruits';

				if(_.some(food, { name })) {
					ingredients.push(name);
					_.pull(found_raw, line);
				}
				else if(_.startsWith(name, 'Any') || _.startsWith(name, 'any')) {
					name = 'A' + name.substr(1).toLowerCase();
					isGeneric = true;
					if(name !== 'Any' && name !== 'Any type of') ingredients.push(name);
					_.pull(found_raw, line);
				}
			});
			if(isGeneric) item.generic = ingredients;
			else item.ingredients = ingredients;

			if(item.method === 'Roasting') {
				utils.verifyCount(ingredients, 1, item.name + ' should have exactly 1 ingredient when Roasting');
				found_raw.splice(0); // if it's roasted and has 1 ingredient, then we should be good
			}
		}

		else if(_.startsWith(item.name, 'Roasted ') && found_raw.length === 1 && found_raw[0] === 'Cooking ingredients') {
			const name = item.name.substr(8);
			if(_.some(food, { name })) {
				item.method = 'Roasting';
				item.ingredients = [name];
				found_raw.splice(0);
			}
		}

		else if((_.startsWith(item.name, 'Baked ') || _.startsWith(item.name, 'Toasty ')) && found_raw.length === 1 && found_raw[0] === 'Cooking ingredients') {
			const name = item.name.substr(item.name.indexOf(' ')+1);
			if(_.some(food, { name })) {
				item.method = 'Roasting';
				item.ingredients = [name];
				found_raw.splice(0);
			}
		}

		else if(_.includes(found_raw, 'Freezing') || _.includes(found_raw, 'Freezing a')) {
			item.method = 'Freezing';
			item.ingredients = [];
			found_raw.forEach((line) => {
				if(_.some(food, { name: line })) {
					item.ingredients.push(line);
				}
			});
			found_raw.splice(0); // it should be handled
			utils.verifyCount(item.ingredients, 1, item.name + ' should have exactly 1 ingredient when Freezing');
		}

		if(item.generic) item.generic = item.generic.sort();
		if(item.ingredients) item.ingredients = item.ingredients.sort();
		switch(item.name) {
			// wrong generics
			case 'Fruit Pie': if(item.generic[0] === 'Any fruit') item.generic[0] = 'Any fruit excluding Apple or Fortified Pumpkin'; break;
			case 'Honeyed Fruits': if(item.generic[0] === 'Any fruit') item.generic[0] = 'Any fruit excluding Apple'; break;

			// wrong names
			case 'Fried Bananas': if(_.isEqual(item.ingredients, ['Cane Sugar', 'Mighty Bananas', 'Tabantha Wheat'])) item.name = 'Mighty Fried Bananas'; break;
			case 'Honeyed Apple': if(_.isEqual(item.ingredients, ['Apple', 'Courser Bee Honey'])) item.name = 'Energizing Honeyed Apple'; break;
			case 'Energizing Meaty Rice Balls': if(_.isEqual(item.ingredients, ['Hylian Rice', 'Raw Meat'])) item.name = 'Meaty Rice Balls'; break;
			case 'Pumpkin Stew': if(_.isEqual(item.ingredients, ['Fortified Pumpkin', 'Fresh Milk', 'Goat Butter', 'Tabantha Wheat'])) item.name = 'Tough Pumpkin Stew'; break;
		}


		if(!_.isEmpty(found_raw)) item.found_raw = found_raw.slice(0); // for debugging (need to comment out the next line, otherwise it throws)
		utils.verifyCount(found_raw, 0, item.name + ' raw found should have been handled, ' + JSON.stringify(found_raw));
		utils.checkInArray(item, 'method', ['Cooking', 'Roasting', 'Freezing', 'Boiling'], item.name);
		// since we are trying to make a standard
		utils.checkIngredients(item, food);
	}
};

// things that I haven't figure out a generic rule for
const HARD_CODED = {
	'Creamy Heart Soup': {
		// Cooking:
		// - Hydromelon
		// - Voltfruit
		// - Hearty Radish or Big Hearty Radish
		// - Fresh Milk
		method: 'Cooking',
		generic: [
			'Hydromelon',
			'Voltfruit',
			'Any Hearty Radish or Big Hearty Radish',
			'Fresh Milk',
		],
	},
	'Fairy Tonic': {
		// Trading beetles with Beedle
		// Cooking:
		// - Fairy
		// - any monster part, Ancient Material, small animal, or gemstone
		method: 'Cooking',
		generic: [
			'Fairy',
			'Any monster part, ancient material, small animal, or gemstone',
		],
	},
	'Fish Pie': {
		// Cooking:
		// -Any Fish or Seafood
		// -Goat Butter
		// -Rock Salt
		// -Tabantha Wheat
		method: 'Cooking',
		generic: [
			'Any seafood',
			'Goat Butter',
			'Rock Salt',
			'Tabantha Wheat',
		],
	},
	'Frozen Bass': {
		// Freezing a Hyrule Bass or Staminoka Bass
		method: 'Freezing',
		generic: [
			'Any Hyrule Bass or Staminoka Bass',
		],
	},
	'Frozen Carp': {
		// Roasting:
		// -Armored Carp
		// -Mighty Carp
		// -Sanke Carp
		method: 'Freezing',
		generic: [
			'Any carp',
		],
	},
	'Frozen Crab': {
		// Freezing any Crab
		method: 'Freezing',
		generic: [
			'Any crab',
		],
	},
	'Frozen Porgy': {
		// Freezing an Armored Porgy or a Mighty Porgy
		method: 'Freezing',
		generic: [
			'Any porgy',
		],
	},
	'Frozen Trout': {
		// Freezing any Trout
		method: 'Freezing',
		generic: [
			'Any trout',
		],
	},
	'Hard-Boiled Egg': {
		// Boiling a Bird Egg
		// West Passage in Hyrule Castle
		method: 'Boiling',
		ingredients: [
			'Bird Egg',
		],
	},
	'Meat and Rice Bowl': {
		// Cooking:
		// -Hylian Rice
		// -Raw Bird Drumstick or Raw Meat
		// -Rock Salt
		// Beedle (Rhino Beetle trading)
		method: 'Cooking',
		generic: [
			'Hylian Rice',
			'Any basic meat',
			'Rock Salt',
		],
	},
	'Prime Poultry Pilaf': {
		// this one is actually perfect, and has a much better format
		// BUT
		// it's no the same as the others, better or no
		// so I don't know how to nicely fold it in
		// maybe there just need to be a second parser or something
		// we can come back to it when there is at least one more like it
		method: 'Cooking',
		ingredients: [
			'Raw Bird Thigh',
			'Bird Egg',
			'Goat Butter',
			'Hylian Rice',
		],
	},
	'Roasted Bass': {
		// Roasting a Hyrule Bass or Staminoka Bass
		// Enemy Campfires
		// Protein Palace
		// Maike at Kara Kara Bazaar
		// Blue Hinox
		method: 'Roasting',
		generic: [
			'Any Hyrule Bass or Staminoka Bass',
		],
	},
	'Roasted Porgy': {
		// Lurelin Village
		// Roasting:
		// -Armored Porgy
		// -Mighty Porgy
		method: 'Roasting',
		generic: [
			'Any porgy',
		],
	},
	'Roasted Tree Nut': {
		// Cooking ingredients
		method: 'Roasting',
		ingredients: [
			'Chickaloo Tree Nut', // not just "tree nut" so it can't find it
		],
	},
	'Seafood Curry': {
		// Cooking:
		// -Any Porgy or Hearty Blueshell Snail
		// -Goron Spice
		// -Hylian Rice
		method: 'Cooking',
		generic: [
			'Any porgy or Hearty Blueshell Snail',
			'Goron Spice',
			'Hylian Rice',
		],
	},
	'Seafood Fried Rice': {
		// Cooking:
		// - any porgy or Hearty Blueshell Snail
		// - Hylian Rice
		// - Rock Salt
		method: 'Cooking',
		generic: [
			'Any porgy or Hearty Blueshell Snail',
			'Hylian Rice',
			'Rock Salt',
		],
	},
	'Steamed Meat': {
		// Cooking meat
		method: 'Cooking',
		generic: [
			'Any meat',
		],
	},
	'Toasty Stamella Shroom': {
		// Cooking Stamella Shroom on the fire
		method: 'Roasting',
		ingredients: [
			'Stamella Shroom',
		],
	},
	'Vegetable Curry': {
		// Cooking:
		// -Fortified Pumpkin or Carrots
		// -Goron Spice
		// -Hylian Rice
		method: 'Cooking',
		generic: [
			'Any carrot or Fortified Pumpkin',
			'Goron Spice',
			'Hylian Rice',
		],
	},
	'Vegetable Risotto': {
		// Cooking:
		// -Fortified Pumpkin or Carrots
		// -Goat Butter
		// -Hylian Rice
		// -Rock Salt
		method: 'Cooking',
		generic: [
			'Any carrot or Fortified Pumpkin',
			'Goat Butter',
			'Hylian Rice',
			'Rock Salt',
		],
	},
	'Veggie Cream Soup': {
		// Cooking:
		// -Any Carrot or Pumpkin
		// -Fresh Milk
		// -Rock Salt
		method: 'Cooking',
		generic: [
			'Any carrot or Fortified Pumpkin',
			'Fresh Milk',
			'Rock Salt',
		],
	},
};
