'use strict';
const _ = require('lodash');
const cheerio = require('cheerio');
const path = require('path');
const lp = require('./load_page');
const utils = require('./utils');

// throttle the requests (since they are cached, we can just increase and run again)
const START_WITH = 0;
const MAX_COUNT = 342;

exports.process = function (path_to_raw, path_to_combined, path_to_images) {
	const images = {
		food: [],
		dishes: [],
	};
	let database;

	// load some other data up front
	return lp.loadJson(path.join(path_to_combined, 'combined.json')).then(function (combined) {
		database = combined;
	}).then(function () {
		return Promise.all([
			lp(path.join(path_to_raw, 'botw-recipes-ingredients.html'), null),
			lp(path.join(path_to_raw, 'botw-recipes-recipes.html'), null),
		]);
	}).then(function ([ingredientPage, recipePage]) {
		const $ipage = cheerio.load(ingredientPage);
		const $rpage = cheerio.load(recipePage);

		// verify page
		utils.verifyCount($ipage('body'), 1, 'dummy check: body');
		utils.verifyCount($ipage('.materialsList'), 1, 'dummy check: has materials');
		utils.verifyCount($ipage('.materialsList img.u-centered'), 149, 'dummy check: material count');
		utils.verifyCount($rpage('body'), 1, 'dummy check: body');
		utils.verifyCount($rpage('.fullRecipesList'), 1, 'dummy check: has recipes');
		utils.verifyCount($rpage('.fullRecipesList img.u-centered'), 132, 'dummy check: recipe count');

		// start parsing
		let count = 0;
		let next = Promise.resolve();

		$ipage('.materialsList img.u-centered').each((idx, el) => {
			count++;
			if(START_WITH > count || count > MAX_COUNT) return;

			el = cheerio(el);
			next = next.then(() => parseMaterial(el, database)).then((item) => {
				if(item) images.food.push(item);
			});
		});
		console.log('  . ', 'finished materials', count);

		// 150 to
		database.dishes.forEach((dish) => {
			// skip these before the count because we don't want them to, uh, count
			if(dish.method === 'Cooking') return;

			count++;
			if(START_WITH > count || count > MAX_COUNT) return;

			next = next.then(() => loadAltMethodImage(dish)).then((item) => {
				if(item) images.dishes.push(item);
			});
		});
		console.log('  . ', 'finished alt methods', count);

		$rpage('.fullRecipesList img.u-centered').each((idx, el) => {
			count++;
			if(START_WITH > count || count > MAX_COUNT) return;

			el = cheerio(el);
			next = next.then(() => parseRecipes(el, database)).then((item) => {
				if(item) images.dishes.push(item);
			});
		});
		console.log('  . ', 'finished recipes', count);

		return next;
	}).then(function () {
		// some stats for double checking
		const missingFood = _.chain(database.food)
			.map('name')
			.pullAll(_.map(images.food, 'name'))
			.value();
		if(missingFood.length) console.log('missing food', missingFood.length, missingFood);
		console.log('  . ', 'food has image', _.filter(images.food, 'localImage').length, '|', 'missing', _.reject(images.food, 'localImage').length);

		// some stats for double checking
		const missingDishes = _.chain(database.dishes)
			.map('name')
			.pullAll(_.map(images.dishes, 'name'))
			.value();
		if(missingDishes.length) console.log('missing dishes', missingDishes.length, missingDishes);
		console.log('  . ', 'dishes has image', _.filter(images.dishes, 'localImage').length, '|', 'missing', _.reject(images.dishes, 'localImage').length);
	}).then(function () {
		return lp.saveJson(images, path.join(path_to_raw, '..', 'images.json'));
	}).then(function () {
		return 'Images Done.';
	});

	function parseMaterial(el) {
		// <img id="" name="" src="/assets/images/stuff/ingredients/1.png" alt="Hearty Durian" title="Hearty Durian" class="u-centered">

		const item = {
			name: el.attr('title'),
			image: 'http://botw-recipes.com' + el.attr('src'),
		};

		const food = _.find(database.food, { name: item.name });
		if(!food) return null;

		const numberMatcher = /(\d+)\.png/.exec(item.image);
		if(!numberMatcher) throw new Error('cannot find number for ' + item.name);
		item.number = +numberMatcher[1];
		utils.checkNumber(item, 'number', 1, 149, item.name);

		// load the image, and then move on
		item.localImage = path.join('images', 'ingredients', item.name+'.png');
		return lp.download(path.join(path_to_images, item.localImage), item.image).then(() => {
			return item;
		});
	}

	function loadAltMethodImage(dish) {
		let suffix = '';
		switch(dish.method) {
			case 'Roasting': suffix = 'roasted'; break;
			case 'Freezing': suffix = 'frozen'; break;
			case 'Boiling': suffix = 'boiled'; break;
			default: throw new Error('method not handled, ' + dish.method + ', ' + dish.name);
		}
		if(!suffix) throw new Error('suffix must be defined; howwww??? ' + dish.method + ', ' + dish.name);

		let ingredient;
		if(_.size(dish.ingredients) !== 1) {
			switch(dish.name) {
				case 'Blackened Crab': ingredient = 'crab'; break;
				case 'Frozen Bass': ingredient = 'bass'; break;
				case 'Frozen Carp': ingredient = 'carp'; break;
				case 'Frozen Crab': ingredient = 'crab'; break;
				case 'Frozen Porgy': ingredient = 'porgy'; break;
				case 'Frozen Trout': ingredient = 'trout'; break;
				case 'Roasted Bass': ingredient = 'bass'; break;
				case 'Roasted Carp': ingredient = 'carp'; break;
				case 'Roasted Porgy': ingredient = 'porgy'; break;
				case 'Roasted Trout': ingredient = 'trout'; break;
				default: throw new Error('cannot compute alt method for this dish ' + dish.name);
			}
		}
		else {
			ingredient = dish.ingredients[0];
		}

		const imageName = (ingredient + ' ' + suffix).replace(/ /g, '-').toLowerCase();
		if(suffix === 'boiled') suffix = 'roasted'; // roasted/bird-egg-boiled.png

		const item = {
			name: dish.name,
			localImage: path.join('images', 'dishes', dish.name+'.png'),
			image: 'https://www.guideofthewild.com/assets/images/cookingPot/ingredients/'+suffix+'/'+imageName+'.png',
		};

		if(true) {
			return lp.download(path.join(path_to_images, item.localImage), item.image).then(() => {
				return item;
			});
		}
		else {
			console.log(item);
			return item;
		}
	}

	function parseRecipes(el) {
		// <img id="" name="" src="/assets/images/stuff/recipes/1.png" alt="Fruitcake" title="Fruitcake" class="u-centered">

		const item = {
			name: el.attr('title'),
			image: 'http://botw-recipes.com' + el.attr('src'),
		};

		// minor inconsistencies
		switch(item.name) {
			case 'Fried Bananas': item.name = 'Mighty Fried Bananas'; break;
			case 'Honey Candy': item.name = 'Energizing Honey Candy'; break;
			case 'Meat and Mushroom Skewer': item.name = 'Meat & Mushroom Skewer'; break;
			case 'Meat-Stuffed Pumpkins': item.name = 'Meat-Stuffed Pumpkin'; break;
			case 'Porgy Meuniere': item.name = 'Porgy Meunière'; break;
			case 'Salmon Meuniere': item.name = 'Salmon Meunière'; break;
			case 'Seafood Meuniere': item.name = 'Seafood Meunière'; break;
			case 'Sauteed Nuts': item.name = 'Sautéed Nuts'; break;
			case 'Sauteed Peppers': item.name = 'Spicy Sautéed Peppers'; break;
			case 'Spicy Peppered Seafood': item.name = 'Pepper Seafood'; break;
		}

		const dish = _.find(database.dishes, { name: item.name });
		if(!dish) {
			if(!_.includes([
				'Fairy Tonic',
				'Rock-Hard Food',
				'Dubious Food',
			], item.name) && !_.endsWith(item.name, 'Elixir'))
				console.log('skipping', item.name);
			return null;
		}

		const numberMatcher = /(\d+)\.png/.exec(item.image);
		if(!numberMatcher) throw new Error('cannot find number for ' + item.name);
		item.number = +numberMatcher[1];
		utils.checkNumber(item, 'number', 1, 132, item.name);

		// load the image, and then move on
		item.localImage = path.join('images', 'dishes', item.name+'.png');
		return lp.download(path.join(path_to_images, item.localImage), item.image).then(() => {
			return item;
		});
	}
};
