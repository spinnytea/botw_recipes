'use strict';
const _ = require('lodash');
const cheerio = require('cheerio');
const path = require('path');
const lp = require('./load_page');
const utils = require('./utils');

// the wiki category is split across 2 pages (page sizes of 200)
// we should be able to detect if page 2 is wrong url because the counts won't line up
const PAGE_1 = 'https://zelda.fandom.com/wiki/Category:Cooking_Ingredients';
const URL_ROOT = 'https://zelda.fandom.com'; // for building links when they aren't absolute paths

// throttle the requests (since they are cached, we can just increase and run again)
const START_WITH = 0;
const MAX_COUNT = 200;


exports.process = function (path_to_raw, path_to_data) {
	const list = [];
	let food = [];

	// load some other data up front
	return lp.loadJson(path.join(path_to_data, 'orcz.json')).then(function (database) {
		food = database.food;
	}).then(function () {
		// load both pages of the categories
		return lp(path.join(path_to_raw, 'ingredients1.html'), PAGE_1);
	}).then(function (page1) {
		const $1 = cheerio.load(page1);

		// verify page
		utils.verifyCount($1('body'), 1, 'dummy check: body');
		utils.verifyCount($1('.mediawiki'), 1, 'dummy check: wiki');
		utils.verifyString(utils.getText($1('p.category-page__total-number')[0]), 'All items (134)', 'total count on page');

		utils.verifyCount($1('li.category-page__member'), 134, 'dummy check: wiki');

		// start parsing
		let count = 0;
		let next = Promise.resolve();
		[$1].forEach(($) => {
			$('li.category-page__member').each((idx, el) => {
				count++;
				if(START_WITH > count || count > MAX_COUNT) return;

				el = cheerio(el);
				next = next.then(() => parseItem(el)).then((item) => {
					if(item) list.push(item);
				});
			});
		});

		return next;
	}).then(function () {
		// spot check some items
		[
			{ name: 'Acorn', found: ['Squirrels', 'Cutting Trees', 'Forested areas'] },
			{ name: 'Armored Carp', found: ['East Necluda', 'Lanayru Great Spring', 'Freshwater rivers, lakes, and ponds'] },
			{ name: 'Dinraal\'s Claw', found: ['Hitting the claws of Dinraal with an Arrow'] },
		].forEach((item) => {
			const actual = _.omit(_.find(list, { name: item.name }), ['localImage', 'image', 'link']);
			if(_.isEmpty(actual)) {
				console.log(item.name + ' was not found in the list');
			}
			else {
				utils.verifyObject(actual, item, 'item ' + item.name + ' does not match');
			}
		});

		// check on the list of food, see if there are any we are missing
		const missingFood = _.pullAll(_.map(food, 'name'), _.map(list, 'name'));
		if(missingFood.length > 0) {
			console.log('missing food', missingFood.length, missingFood);
		}
	}).then(function () {
		return lp.saveJson(list, path.join(path_to_data, 'fandom_ingredients.json'));
	}).then(function () {
		return 'Fandom Ingredients Done.';
	});

	function parseItem(el) {
		utils.verifyCount(el.find('>a'), 1, 'only 1 link per item');

		const item = {
			name: utils.getText(el.find('>a')),
			link: URL_ROOT + el.find('>a').attr('href'),
		};

		// minor inconsistencies
		if(item.name === 'Fairy (Breath of the Wild)') item.name = 'Fairy';

		// for now, we only care about a limited test of items
		// maybe this will be lifted in the future
		if(!_.some(food, { name: item.name })) {
			return Promise.resolve(null);
		}

		return lp(path.join(path_to_raw, '/ingredients_pages/'+item.name+'.html'), item.link).then(function (page) {
			const $ = cheerio.load(page);
			const $info = $('aside.portable-infobox');

			if(item.name !== 'Fairy') {
				utils.verifyString(utils.getText($('.page-header__title')), item.name, 'page name should match item name');
			}
			utils.verifyCount($info, 1, 'dummy check: page info box');
			utils.verifyString(utils.getText($info.find('h2')), item.name, 'info name should match item name');

			handleFound(item, $info);

			item.image = $info.find('.pi-image img').attr('src');

			// if there is no image, there's nothing we can do here
			if(!item.image) return Promise.resolve(item);

			utils.checkString(item, 'image', item.name);
			if(!_.startsWith(item.image, 'https://vignette.wikia.nocookie.net/zelda/images/')) throw new Error('image src looks wrong, perfix, ' + item.name);
			if(!_.includes(item.image, '.png')) {
				if(item.name === 'Silent Princess') {
					// this is probably because it's the game's namesake
					// it's a larger image than just an in-game icon
					delete item.image;
					return item;
				}
				throw new Error('image src looks wrong, type, ' + item.name);
			}

			// load the image, and then move on
			// item.localImage = path.join('images', 'ingredients', item.name+'.png');
			// return lp.download(path.join('botw_recipes_vue/public', item.localImage), item.image, null).then(() => {
			// 	return item;
			// });

			return item;
		});
	}

	function handleFound(item, $info) {
		if(item.name in HARD_CODED) {
			_.assign(item, HARD_CODED[item.name]);
		}
		else {
			const found_raw = utils.getTextList($info.find('.pi-data-label:contains("Found")+.pi-data-value')[0], { splitComma: false });
			utils.checkString({ found_raw }, 'found_raw', item.name);

			// minor inconsistencies
			// really, just parsing issues; getTextLine is eager when it splits
			utils.replaceList(found_raw, ['"', 'The Korok Trials', '" reward'], 'Reward for "The Korok Trials"');
			utils.replaceList(found_raw, ['"', 'The Search for Barta', '"', 'Quest', 'Reward'], 'Reward for "The Search for Barta"');
			utils.replaceList(found_raw, ['"', 'A Wife Washed Away', '"'], 'Reward for "A Wife Washed Away"');
			utils.joinList(found_raw, ['Occasionally dropped by', 'Cucco']);
			utils.joinList(found_raw, ['Hitting the claws of', 'Dinraal', 'with an Arrow']);
			utils.joinList(found_raw, ['Hitting the claws of', 'Farosh', 'with an Arrow']);
			utils.joinList(found_raw, ['Hitting the mouth of', 'Farosh', 'with an Arrow']);
			utils.joinList(found_raw, ['Hitting the main body of', 'Naydra']);
			utils.joinList(found_raw, ['Cutting grass in', 'East Necluda']);
			utils.joinList(found_raw, ['Trading', 'Goron Spice', 'to', 'Lester']);
			utils.joinList(found_raw, ['Reward for saving', 'Chabi', 'from monsters']);
			utils.joinList(found_raw, ['Pigeons', '(all subspecies save for Wood)']);
			utils.joinList(found_raw, ['Dining Hall', 'of', 'Hyrule Castle']);
			utils.joinList(found_raw, ['East Passage', 'in', 'Hyrule Castle']);
			utils.joinList(found_raw, ['Breaking', 'Ore Deposits']);
			utils.joinList(found_raw, ['Cottla', '(mini-game reward)']);
			utils.replaceList(found_raw, ['Rito Village', '\'s', 'Slippery Falcon'], 'Slippery Falcon');
			utils.joinList(found_raw, ['Cutting grass all over', 'Tabantha Frontier']);
			utils.joinList(found_raw, ['Cacti in', 'Gerudo Desert']);

			if(_.min(_.map(found_raw, 'length')) < 3 || _.some(found_raw, (f) => _.startsWith(f, '('))) {
				console.log(found_raw);
				throw new Error('Some of the locations are suspiciously short, ' + item.name);
			}

			// XXX what I'm really intersted in is "are these prepositions", "from, by, of, etc."
			//  - like, ideally, these would all be "Noun" or "Verb Noun" or "Verb <colorfoul language> Noun"
			// REIVEW so for now, we can throw some simple errors, but really, you just gotta check the result manually
			if(_.min(_.map(_.map(found_raw, (f) => _.last(f.split(' '))), 'length')) < 3) {
				console.log(found_raw);
				throw new Error('The last word of this found seems suspiciously short, ' + item.name);
			}
			if(_.min(_.map(_.map(found_raw, (f) => _.first(f.split(' '))), 'length')) < 3) {
				console.log(found_raw);
				throw new Error('The first word of this found seems suspiciously short, ' + item.name);
			}

			// for now, we can just use this list directly
			// TODO we probably need to sanitize this somehow
			item.found = found_raw;
		}

		utils.checkString(item, 'found', item.name);
	}
};

// things that I haven't figure out a generic rule for
const HARD_CODED = {
	'Apple': {
		// (A Link to the Past)
		// In trees
		// (Twilight Princess)
		// Hyrule Castle Town Market
		// (A Link Between Worlds)
		// In trees
		// (Breath of the Wild)
		// Apple Trees
		// Barrels
		// Crates
		// Gartan
		// General Shoppe
		// Hinox
		found: [
			'Apple Trees',
			'Barrels',
			'Crates',
			'Gartan',
			'General Shoppe',
			'Hinox',
		],
	},
	'Cool Safflina': {
		// does not have a found section
		found: [
			'Hebra Mountains',
			'Gerudo Highlands',
			'Surrounding the Great Fairy Fountain in the Gerudo Wasteland',
		],
	},
	'Hyrule Bass': {
		// (Twilight Princess)
		// Fishing Hole
		// Eldin Spring
		// Goron Mines
		// (Breath of the Wild)
		// Throughout Hyrule
		found: ['Throughout Hyrule'],
	},
	'Raw Whole Bird': {
		// Dropped by Eldin Ostriches & White Pigeons
		// Dining Hall of Hyrule Castle
		found: [
			'Ostriches',
			'White Pigeons',
			'Dining Hall of Hyrule Castle',
		],
	},
	'Sanke Carp': {
		// West Necluda:
		// -Kakariko Village
		// -Lantern Lake
		found: [
			'Kakariko Village',
			'Lantern Lake',
		],
	},
	'Star Fragment': {
		// does not have a found section
		// it is a bit complicated
		found: [
			'They appear at night (obviously), somewhere between 9 p.m. and 3 a.m.',
			'They fall somewhere in your line of sight — in other words, wherever you’re looking when they appear. This means you can kind of control where one lands.',
		],
	},
};