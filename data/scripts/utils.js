'use strict';
const _ = require('lodash');
const cheerio = require('cheerio');

exports.checkString = function (item, prop, category) {
	var p = item[prop];
	// check single string (also works for arrays)
	if(!p || !(_.isString(p) || _.isArray(p)) || !p.length)
		throw new Error(prop + ' in ' + category + ' is not a string, ' + p + ', ' + JSON.stringify(item));
	// check every item in array
	if(_.isArray(p) && _.some(p, (s)=>(!s || !_.isString(s) || !s.length)))
		throw new Error(prop + ' array in ' + category + ' is not a list of strings, ' + p + ', ' + JSON.stringify(item));
};

exports.checkNumber = function (item, prop, min, max, category) {
	var p = item[prop];
	// check single string (also works for arrays)
	if(!_.isNumber(p) || isNaN(p))
		throw new Error(prop + ' in ' + category + ' is not a number, ' + p + ', ' + JSON.stringify(item));
	if(p < min || p > max)
		throw new Error(prop + ' in ' + category + ' is out of range, '+min+' < '+p+' < '+max+', ' + JSON.stringify(item));
};

exports.checkBoolean = function (item, prop, category) {
	var p = item[prop];
	// check single string (also works for arrays)
	if(!_.isBoolean(p))
		throw new Error(prop + ' in ' + category + ' is not a boolean, '+p+', ' + JSON.stringify(item));
};

exports.checkInArray = function (item, prop, array, category) {
	var p = item[prop];
	// check single string (also works for arrays)
	if(!p || !_.includes(array, p))
		throw new Error(prop + ' in ' + category + ' is not a specified value, ' + p + ', '+JSON.stringify(array)+', ' + JSON.stringify(item));
};

// for multiple selector matches, get each text value
exports.mapText = function ($query) {
	return $query.toArray().map((el)=>exports.getText(el));
};

exports.getText = function (elem) {
	return cheerio(elem).text().trim().replace(/\s+/g, ' ').replace('’', '\'');
};

// for a single item that contains multiple text objects
exports.getTextList = function (elem, { splitComma=true }={}) {
	return _.chain(elem.children)
		.map(exports.getText)
		.map((t) => (splitComma ? t.split(',') : t))
		.flatten()
		.map((t) => _.trim(t, '. '))
		.compact()
		.value();
};

// check a query length
exports.verifyCount = function ($query, count, message) {
	if($query.length !== count) throw new Error(message + ': ' + $query.length + ' of ' + count);
};

// check the text of each query result
exports.verifyText = function ($query, expected) {
	const actual = exports.mapText($query);

	if(!_.isEqual(actual, expected)) throw new Error([
		'unexpected text values',
		'actual',
		JSON.stringify(actual),
		'expected',
		JSON.stringify(expected),
	].join(', '));
};

exports.verifyString = function (actual, expected, message) {
	if(_.isArray(actual) && _.isArray(expected)) {
		const a = _.difference(actual, expected);
		const e = _.difference(expected, actual);
		if(a.length !== 0 || e.length !== 0)
			throw new Error([
				message,
				'[' + a.join(', ') + ']',
				'[' + e.join(', ') + ']',
			].join('\n'));
	}
	else {
		if(actual !== expected)
			throw new Error([
				message,
				actual,
				expected,
			].join(','));
	}
};

// compare 2 json objects
exports.verifyObject = function (actual, expected, message) {
	if(!_.isEqual(actual, expected)) {
		// remove values that are the same
		// this will highlight the errors
		actual = _.cloneDeep(actual);
		expected = _.cloneDeep(expected);
		_.keys(expected).forEach((k)=>{
			if(_.isEqual(actual[k], expected[k])) {
				delete actual[k];
				delete expected[k];
			}
		});

		throw new Error([
			message,
			JSON.stringify(actual),
			JSON.stringify(expected),
		].join('\n'));
	}
};

exports.replaceList = function (array, match, replacement) {
	const idx = array.indexOf(match[0]);
	if(_.isEqual(array.slice(idx, idx+match.length), match)) {
		array.splice(idx, match.length, replacement);
	}
};
exports.joinList = function (array, match) {
	const idx = array.indexOf(match[0]);
	if(_.isEqual(array.slice(idx, idx+match.length), match)) {
		array.splice(idx, match.length, match.join(' '));
	}
};

const ANY = Object.freeze([
	'Any crab',
	'Any carrot',
	'Any mushroom',
	'Any meat', // any of the 6 raw meat/bird
	'Any basic meat', // Raw Meat or Raw Bird Drumstick
	'Any prime meat', // Raw Prime Meat or Raw Bird Thigh
	'Any gourmet meat', // Raw Gourmet Meat or Raw Whole Bird
	'Any seafood',
	'Any fish',
	'Any snail or crab',
	'Any carp',
	'Any porgy',
	'Any trout',
	'Any fruit',
	'Any nut',
	'Any vegetable or herb',
	'Any variety of four different fish',
	'Any variety of four different meats',
	'Any variety of four different mushrooms',
	'Any variety of four different fruits',
	'Any variety of four different vegetables or herbs',

	// these are very specific
	// but they can also be generated later
	'Any Hearty Radish or Big Hearty Radish',
	'Any Hyrule Bass or Staminoka Bass',
	'Any porgy or Hearty Blueshell Snail',
	'Any carrot or Fortified Pumpkin',
	'Any seafood excluding Hearty Salmon or Porgy',
	'Any fruit excluding Apple or Fortified Pumpkin',
	'Any fruit excluding Apple',
	'Any Wildberry or Apple',
]);

exports.checkIngredients = function (item, food) {
	if(item.hasOwnProperty('ingredients')) exports.checkString(item, 'ingredients', item.name);
	if(item.hasOwnProperty('generic')) exports.checkString(item, 'generic', item.name);
	if(!item.hasOwnProperty('ingredients') && !item.hasOwnProperty('generic')) {
		throw new Error(item.name + ' must have either ingredients or generic');
	}
	if(item.hasOwnProperty('ingredients') && item.hasOwnProperty('generic')) {
		throw new Error(item.name + ' must have either ingredients or generic, not both');
	}

	if(item.ingredients) item.ingredients.forEach((i) => {
		if(!_.some(food, { name: i })) throw new Error(i + ' is not a valid ingredient, ' + item.name);
	});
	let any = false;
	if(item.generic) {
		item.generic.forEach((i) => {
			if(!_.startsWith(i, 'Any')) {
				if(!_.some(food, { name: i })) throw new Error(i + ' is not a valid ingredient, ' + item.name);
			}
			else {
				any = true;
				if(!_.includes(ANY, i)) throw new Error(i + ' is not an approved generic case, ' + item.name);
			}
		});
		if(!_.isEqual(item.generic, _.uniq(item.generic))) {
			console.log(item);
			throw new Error('generic recipes should not include duplicate items, ' + item.name);
		}

		// generic list doesn't HAVE to only 'any' in it
		// if the "top level recipe" isn't generic, then use the key "ingredients" instead
		// e.g. Omelet; the general recipe is "an egg", but you can add stuff to it anyway
		if(!any) {
			throw new Error('does not have an "Any", should be an ingredients list, ' + item.name + ', ' + JSON.stringify(item.generic));
		}
	}
	if(item.examples) {
		if(_.isEmpty(item.examples)) {
			// examples cannot be empty
			// unless ...
			// unless the generic case contains only food (if there is one and only one actual recipe)
			// (you can omit examples, if the generic case is actually a specific case)
			if(_.isEmpty(item.ingredients) || !_.isEmpty(item.generic)) {
				throw new Error('examples cannot be empty, ' + item.name);
			}
		}
		item.examples.forEach((e) => {
			if(e.hasOwnProperty('generic')) throw new Error('examples cannot have generics');
			if(e.hasOwnProperty('examples')) throw new Error('examples cannot have examples');
			exports.checkIngredients(e, food);
		});
	}
};
