'use strict';
const _ = require('lodash');
const path = require('path');
const lp = require('./load_page');
const utils = require('./utils');

const SKIPPED_NAMES = require('./scrape_fandom_dishes').SKIPPED_NAMES.slice(0);
const ALL_PREFIXES = [
	// cooking methods
	['Baked', 'method', 'Roasting'],
	['Blackened', 'method', 'Roasting'],
	['Campfire', 'method', 'Roasting'],
	['Charred', 'method', 'Roasting'],
	['Frozen', 'method', 'Freezing'],
	['Frozen', 'effect', 'Chilly'],
	['Icy', 'method', 'Freezing'],
	['Icy', 'effect', 'Chilly'],
	['Roasted', 'method', 'Roasting'],
	['Toasted', 'method', 'Roasting'],
	['Toasty', 'method', 'Roasting'],

	// effects ... just compute it below because the effects/prefixes are consitent
	// ['Electro', 'effect', 'Electro'],
];
const ALL_METHODS = ['Cooking', 'Roasting', 'Freezing', 'Boiling'];
const EFFECT_NAMES = [];
const STANDARD_SUMMARY_PREFIX = '  .';


/**
 * Here is the intended goal:
 *  - each recipe can have a single exact recipe - OR - a generic recipe with a list of exact examples (at least 1)
 */
exports.process = function (path_to_data, path_to_output) {
	const database = {};

	return Promise.all([
		lp.loadJson(path.join(path_to_data, 'orcz.json')),
		lp.loadJson(path.join(path_to_data, 'ign.json')),
		lp.loadJson(path.join(path_to_data, 'fandom_dishes.json')),
		lp.loadJson(path.join(path_to_data, 'fandom_ingredients.json')),
		lp.loadJson(path.join(path_to_data, 'images.json')),
		lp.loadJson(path.join(path_to_data, 'myhand_dishes.json')),
		lp.loadJson(path.join(path_to_data, 'myhand_anys.json')),
		lp.loadJson(path.join(path_to_data, 'myhand_effects.json')),
		lp.loadJson(path.join(path_to_data, 'myhand_ingredients.json')),
	]).then(function ([orcz, ign, fandom_dishes, fandom_ingredients, images, myhand_dishes, anys, myhand_effects, myhand_ingredients]) {
		// console.log('orcz', _.flatten(_.map(orcz, (list, k) => [k, list.length])));
		// console.log('orcz', _.chain(orcz.dishes).map(_.keys).flatten().countBy(_.identity).map((v, k) => [k, v]).flatten().value());
		// console.log('ign', ign.length);
		// console.log('ign', _.chain(ign).map(_.keys).flatten().countBy(_.identity).map((v, k) => [k, v]).flatten().value());
		// console.log('fandom_dishes', fandom_dishes.length);
		// console.log('fandom_dishes', _.chain(fandom_dishes).map(_.keys).flatten().countBy(_.identity).map((v, k) => [k, v]).flatten().value());
		// console.log('hand', hand.length);
		// console.log('hand', _.chain(hand).map(_.keys).flatten().countBy(_.identity).map((v, k) => [k, v]).flatten().value());
		database.food = orcz.food;
		database.effects = orcz.effects;
		database.anys = anys;

		// since this is the only place it's actually used, lets just check this now to be sure
		myhand_dishes.forEach((item) => { if(item.ingredients) utils.checkIngredients(item, database.food); });

		// add effects to the standard prefixes
		Array.prototype.push.apply(EFFECT_NAMES, _.map(database.effects, 'name'));
		EFFECT_NAMES.forEach((name) => ALL_PREFIXES.push([name, 'effect', name]));
		database.effects.forEach((effect) => {
			_.assign(effect, myhand_effects[effect.name]);
			utils.checkString(effect, 'name', 'UNKOWN EFFECT NAME');
			utils.checkString(effect, 'description', effect.name);
			utils.checkNumber(effect, 'maxPotency', 2, 25, effect.name);
			utils.checkString(effect, 'localImage', effect.name);
		});

		let dishes = _.flatten([orcz.dishes, ign, fandom_dishes, images.dishes, myhand_dishes]);
		dishes.forEach((dish) => {
			if(dish.ingredients) dish.ingredients = dish.ingredients.sort();
			if(dish.generic) dish.generic = dish.generic.sort();
		});
		// console.log('all', dishes.length);
		// console.log('all', _.chain(dishes).map(_.keys).flatten().countBy(_.identity).map((v, k) => (k + ': ' + v)).flatten().value());

		// since this is the only place it's actually used, lets just check this now to be sure
		expandAnys(database, dishes);

		handleFood(database.food, _.flatten([fandom_ingredients, myhand_ingredients, images.food]));

		console.log(STANDARD_SUMMARY_PREFIX, 'unique foods', database.food.length);
		console.log(STANDARD_SUMMARY_PREFIX, 'original recipe list', dishes.length);

		dishes = _.chain(dishes)
			.forEach(calcBaseName)
			.sortBy('baseName')
			.groupBy('baseName')
			.mapValues((examples, name) => ({ name, examples }))
			.forEach(extractValues)
			.omitBy((dish) => (!_.some(dish.examples, 'ingredients') && !_.some(dish.examples, 'generic') && _.includes(SKIPPED_NAMES, dish.name) ))
			.forEach((dish) => handleIngredients(dish, anys))
			.forEach(validateFinalExamples)
			.forEach(validateFinalDish)
			.forEach(validateExamplesWithGeneric)
			.value();

		validateTieredMeat(dishes);

		console.log(STANDARD_SUMMARY_PREFIX, 'number of dishes', _.size(dishes));
		console.log(STANDARD_SUMMARY_PREFIX, 'unique recipes', _.chain(dishes).map('examples').flatten().size().value());

		return dishes;
	}).then(function (dishes) {
		// check for punctuation differences in names
		const nameDups = _.chain(dishes)
			.keys()
			.map((n) => n.replace(/[ -]/g, '').toLowerCase())
			.map((n) => n.replace(/[éè]/g, 'e'))
			.map((n) => n.replace(/[&]/g, 'and'))
			// .map((n) => n.split('').sort().join('')) // bonus: letters out of order
			.countBy(_.identity)
			.map((c, k) => (c > 1 ? k : null))
			.compact()
			.value();
		if(!_.isEmpty(nameDups)) throw new Error(JSON.stringify(nameDups));

		// stats on dishes; which properties are used
		// const keys = _.chain(dishes).values().map(_.keys).flatten().sort().sortedUniq().value();
		// const maxKeyLength = _.chain(keys).map(_.size).max().value();
		// keys.forEach((prop) => {
		// 	const has = _.padStart(_.reject(dishes, (d) => d[prop] === null || d[prop] === undefined).length, 3);
		// 	const hasnot = _.padStart(_.filter(dishes, (d) => d[prop] === null || d[prop] === undefined).length, 3);
		// 	const highlight = (+hasnot && !_.includes(['effect', 'examples', 'generic', 'ingredients', 'notes'], prop) ? '*' : '');
		// 	console.log(_.padEnd(prop, maxKeyLength), 'without', hasnot, 'with', has, highlight);
		// });

		// remove skipped that are actually dishes
		_.pullAll(SKIPPED_NAMES, _.keys(dishes));
		// remove skipped that I simply don't care about
		_.pullAll(SKIPPED_NAMES, ['Dubious Food', 'Rock-Hard Food']); // REVIEW should we ignore these?
		// remove skipped if prefixed and has base recipe
		_.remove(SKIPPED_NAMES, (name) => _.some(ALL_PREFIXES, ([prefix]) => (_.startsWith(name, prefix) && dishes[name.substr(prefix.length+1)])));
		if(!_.isEmpty(SKIPPED_NAMES)) console.log('skipped and unknown', SKIPPED_NAMES.length, SKIPPED_NAMES);

		database.dishes = _.values(dishes);
	}).then(function () {
		database.food = _.sortBy(database.food, 'name');
		database.effects = _.sortBy(database.effects, 'name');
		database.dishes = _.sortBy(database.dishes, 'name');

		// sort ingredients by number
		//  - name is easier/simpler for all of the logic
		//  - but number is what we actually want for the final product
		const foodMap = _.mapValues(_.keyBy(database.food, 'name'), 'number');
		function sortByNumber(item, name) {
			if(item[name]) item[name] = _.sortBy(item[name], (i) => foodMap[i] || i);
		}
		_.forEach(database.anys, (list, name) => database.anys[name] = _.sortBy(list, (i) => foodMap[i]));
		database.dishes.forEach((dish) => {
			sortByNumber(dish, 'ingredients');
			sortByNumber(dish, 'generic');
			dish.examples.forEach((e) => {
				sortByNumber(e, 'ingredients');
			});
		});

		return lp.saveJson(database, path.join(path_to_output, 'combined.json'));
	}).then(function () {
		return 'Finished combining.';
	});

	function validateFinalDish(dish) {
		utils.checkString(dish, 'name', 'UNKOWN NAME');
		utils.checkInArray(dish, 'method', ALL_METHODS, dish.name);
		utils.checkString(dish, 'localImage', dish.name);
		utils.checkInArray(dish, 'category', [
			'Desserts',
			'Monster Foods',
			'Poultry and Meat Entrees',
			'Seafood Entrees',
			'Sides and Snacks',
			'Surf and Turf Entrees',
			'Vegetarian Entrees',

			// REVIEW remove
			'Roasting',
			'Freezing',
			'Boiling',
		], dish.name);

		if(!dish.hasOwnProperty('generic')) throw new Error('should have generic');
		if(!dish.hasOwnProperty('examples')) throw new Error('should have a examples');
		if(dish.hasOwnProperty('ingredients')) throw new Error('should not have exact ingredients');
		if(dish.hasOwnProperty('effect')) throw new Error('generic dish should not have an effect');

		// some special case cleanup
		// if there is only one example and it's exacty the same as it's generic, then just skip the generic
		// ... actually, we can do this in the ui
		//     it'd be nice if we could cut down on data, but that's proving difficult to validate
		// if(dish.examples.length === 1 && _.isEqual(dish.generic.sort(), dish.examples[0].ingredients.sort()) &&
		// 	!dish.examples[0].effect && !dish.examples[0].notes) {
		// 	dish.examples.splice(0);
		// }
		// use 'ingredients' instead of 'generic'
		if(!_.some(dish.generic, (i) => _.startsWith(i, 'Any '))) {
			dish.ingredients = dish.generic;
			delete dish.generic;
		}

		// check the generic
		// which also checks examples
		utils.checkIngredients(dish, database.food);

		const unknownKeys = _.pullAll(_.keys(dish), ['name', 'localImage', 'method', 'category', 'generic', 'ingredients', 'examples']);
		if(unknownKeys.length) throw new Error(dish.name + ' has some keys that are not accounted for: ' + unknownKeys);
	}

	function validateFinalExamples(_dish_) {
		_dish_.examples.forEach((example) => {
			const category = _dish_.name + ' -> ' + example.name;

			utils.checkString(example, 'name', category);
			utils.checkIngredients(example, database.food);
			if(example.effect === undefined) example.effect = null;
			if(example.effect !== null) utils.checkInArray(example, 'effect', _.map(database.effects, 'name'), category);
			if(example.notes) {
				example.notes = example.notes
					.replace('Open flame. ', '')
					.replace('(Open flame) - ', '');
				if(example.notes === 'Open flame.') delete example.notes;
				if(/open|flame/i.test(example.notes)) throw new Error('no more open flame');
			}
			if(example.notes) {
				if(!_.isString(example.notes)) throw new Error('notes should be a string, not ' + (typeof example.notes));
				utils.checkString(example, 'notes', category);
				example.notes = _.trim(example.notes, '\\');
			}

			const unknownKeys = _.pullAll(_.keys(example), ['name', 'ingredients', 'notes', 'effect']);
			if(unknownKeys.length) throw new Error(category + ' has some keys that are not accounted for: ' + unknownKeys);
		});
	}

	//  - e.g. Poultry Curry, Prime Poultry Curry, Gourmet Poultry Curry
	function validateTieredMeat(dishes) {
		const tieredDishNames = _.chain(dishes)
			.keys()
			.map((n) => (/^(Prime|Gourmet) (.*)$/.exec(n)||[])[2])
			.compact()
			.sort()
			.sortedUniq()
			.value();

		const tiers = _.chain(dishes)
			.filter((dish) => _.some(tieredDishNames, (n) => _.endsWith(dish.name, n)))
			.groupBy((dish) => (/^(Prime |Gourmet )?(.*)$/.exec(dish.name)||[])[2])
			.value();
		// special case; it doesn't fit the name standards
		tiers['Salt-Grilled Meat'] = [
			dishes['Salt-Grilled Meat'],
			dishes['Salt-Grilled Prime Meat'],
			dishes['Salt-Grilled Gourmet Meat'],
		];

		// these will be removed as we go
		const nonTierDishNames = _.keys(dishes);

		_.forEach(tiers, (tier, name) => {
			// all tiers should have all three
			utils.verifyCount(tier, 3, 'all tiers must have all three, ' + name);
			const gourmet = _.remove(tier, (dish) => _.includes(dish.name, 'Gourmet'))[0];
			const prime = _.remove(tier, (dish) => _.includes(dish.name, 'Prime'))[0];
			const basic = tier.splice(0)[0];
			if(!gourmet) throw new Error('Could not find gourmet for ' + name);
			if(!prime) throw new Error('Could not find prime for ' + name);
			if(!basic) throw new Error('Could not find basic for ' + name);

			_.pullAll(nonTierDishNames, [gourmet.name, prime.name, basic.name]);

			[[gourmet, 'gourmet'], [prime, 'prime'], [basic, 'basic']].forEach(([dish, type]) => {
				switch(name) {
					// teirs should have generic
					// teirs should use "any basic/prime/gourmet meat"
					default: {
						utils.checkString(dish, 'generic', 'verify generic recipe for ' + dish.name);
						const ingredient = `Any ${type} meat`;
						if(!_.includes(dish.generic, ingredient)) {
							throw new Error(dish.name + ' does not include ' + ingredient +
								(_.includes(dish.generic, 'Any meat') ? '; uses Any meat' : ''));
						}
						break;
					}
					// these are "raw meat" specific versions
					case 'Meat Curry':
					{
						utils.checkString(dish, 'ingredients', 'verify ingredients recipe for ' + dish.name);
						const ingredient = { 'basic': 'Raw Meat', 'prime': 'Raw Prime Meat', 'gourmet': 'Raw Gourmet Meat' }[type];
						if(!_.includes(dish.ingredients, ingredient)) throw new Error(dish.name + ' does not include ' + ingredient);
						break;
					}
					// these are "raw bird" specific versions
					case 'Poultry Curry':
					case 'Poultry Pilaf':
					{
						utils.checkString(dish, 'ingredients', 'verify ingredients recipe for ' + dish.name);
						const ingredient = { 'basic': 'Raw Bird Drumstick', 'prime': 'Raw Bird Thigh', 'gourmet': 'Raw Whole Bird' }[type];
						if(!_.includes(dish.ingredients, ingredient)) throw new Error(dish.name + ' does not include ' + ingredient);
						break;
					}
				}
			});
		});

		// non-tears should use "any meat"
		nonTierDishNames.forEach((name) => {
			const dish = dishes[name];
			if(dish.generic) {
				if(_.some(['Any basic meat', 'Any prime meat', 'Any gourmet meat'], (i) => _.includes(dish.generic, i))) {
					throw new Error('Non-tiered dishes should use Any meat instead of something more specific, ' + name);
				}
			}
		});
	}

	function validateExamplesWithGeneric(dish) {
		if(!dish.examples.length) {
			if(dish.generic || !dish.ingredients) {
				console.log(dish);
				throw new Error('if dish does not have examples, then it must have an exact recipe, ' + dish.name);
			}

			// if there are no examples, then there's nothing to do here
			return;
		}

		// these copious ones are special
		if(_.startsWith(dish.name, 'Copious ') && _.size(dish.generic) === 1 && _.startsWith(dish.generic[0], 'Any variety of four different'))
			return;

		const base = (dish.generic || dish.ingredients).map((i) => {
			if(_.startsWith(i, 'Any ')) {
				return database.anys[i];
			}
			else {
				return [i];
			}
		});

		// each example must have something from each of the slots in base
		dish.examples.forEach((e) => {
			const ingredients = e.ingredients.slice(0);
			if(ingredients.length < base.length) {
				throw new Error('the generic example should be the simplest, ' + dish.name);
			}

			// it's okay if the ingredients list is longer
			// if it is, then the ingredients list will HAVE to have extra things
			// the import part is that all the items in the generic recipe are in the examples
			base.forEach((set) => {
				const i_idx = _.findIndex(ingredients, (i) => _.includes(set, i));
				if(i_idx !== -1) {
					ingredients.splice(i_idx, 1); // don't reuse the same ingredient
				}
				else {
					console.log(dish.name, (dish.generic || dish.ingredients), e);
					throw new Error('example does not match generic, ' + dish.name);
				}
			});
		});
	}
};

function calcBaseName(dish, ignore, dishes) {
	if(_.includes([
		// generic versions doesn't exist
		'Energizing Honey Candy', // just honey
		'Mighty Fried Bananas', // just bananas
		'Sneaky River Escargot', // Roasted
		'Spicy Sautéed Peppers', // just peppers
	], dish.name)) {
		dish.baseName = dish.name;
	}
	else if(_.some(EFFECT_NAMES, (prefix) => _.startsWith(dish.name, prefix))) {
		dish.baseName = dish.name.substr(dish.name.indexOf(' ') + 1);
		if(dish.baseName === 'Milk') dish.baseName = 'Warm Milk';
	}
	else {
		// no prefix
		dish.baseName = dish.name;
	}

	// this was a further refinement from before, but, it's not actually
	// these are different/specific recipes
	// if(_.startsWith(dish.baseName, 'Prime ')) dish.baseName = dish.baseName.substr(6);
	// if(_.startsWith(dish.baseName, 'Gourmet ')) dish.baseName = dish.baseName.substr(8);

	utils.checkString(dish, 'baseName', dish.name);
	if(!_.some(dishes, { name: dish.baseName })) {
		throw new Error(dish.name + ' does not have a generic version');
	}
}

function extractValues(dish) {
	dish.examples.forEach((example) => {
		// check defaults based on what the prefix implies
		_.forEach(ALL_PREFIXES, ([prefix, prop, value]) => {
			if(_.startsWith(example.name, prefix)) {
				if(example[prop] === null || example[prop] === undefined) example[prop] = value;
				if(example[prop] !== value) throw new Error('conflicting '+[prefix, value].join('/')+', ' + dish.name + ' -> ' + example.name + ', ' + example[prop]);
				// return false; // we can't exit early because the prefixes aren't unique; some effect multiple props
			}
		});
	});

	dish.category = collectProperty(dish, 'category');
	dish.method = collectProperty(dish, 'method');
	dish.localImage = collectProperty(dish, 'localImage', { base: true });

	if(dish.method !== 'Cooking') {
		if(dish.category) {
			if(dish.name !== 'Charred Pepper')
				throw new Error('unexpected category for ' + dish.name + ', ' + dish.category);
		}
		dish.category = dish.method;
	}

	dish.examples.forEach((example) => {
		// promoted
		delete example.category;
		delete example.method;
		delete example.localImage;

		// note needed anymore
		delete example.baseName;
		delete example.link; // (original url)
		delete example.image; // (original url)

		// this isn't on all of the examples, and it's something we should/could compute later
		delete example.hearts;
		delete example.strength;
	});
}

function collectProperty(obj, prop, { list=false, base=false }={}) {
	const values = _.chain(obj.examples || obj.sources)
		.filter((s) => !base || obj.name === s.name)
		.map(prop)
		.pull('-')
		.compact()
		.uniq()
		.value();

	if(list) return (values.length > 0 ? values : null);

	if(values.length === 0) {
		if(base) return collectProperty(obj, prop);
		return null;
	}
	if(values.length === 1) {
		return values[0];
	}
	else if(values.length > 1) {
		throw new Error('examples with conflicting '+prop+', ' + obj.name + ', ' + values);
	}
}

function expandAnys(database, dishes) {
	// console.log(JSON.stringify(_.map(database.food, 'name'), null, 2));
	// console.log(JSON.stringify(_.map(_.filter(database.food, f => f.effect === 'Hearty'), 'name'), null, 2));
	// console.log(JSON.stringify(_.map(database.food, 'name').filter((name) => _.includes(name, 'Trout'))));

	const anys = database.anys;
	const anysText = database.anysText = {};
	// keep the recursive text from anys
	_.forEach(anys, (list, a) => {
		anysText[a] = [a].concat(list.filter(i => _.startsWith(i, 'Any ')).sort());
	});
	// I think this hubaloo is just because seafood has fish
	let anysTextPrev;
	do {
		anysTextPrev = _.cloneDeep(anysText);
		_.forEach(anysText, (list, a) => {
			anysText[a] = _.chain(list)
				.map((i) => anysText[i])
				.flatten()
				.concat(list)
				.sort()
				.sortedUniq()
				.value();
		});
	} while(!_.isEqual(anysTextPrev, anysText));

	// remove recursion from anys
	_.forEach(anys, (list, a) => {
		anys[a] = _.flatten(list.map((i) => {
			if(_.startsWith(i, 'Any ')) {
				return anys[i];
			}
			else {
				return i;
			}
		})).sort();
	});
	_.forEach(anys, (list) => {
		if(list.some(i => _.startsWith(i, 'Any ')))
			throw new Error('anys are still recursive, expand them again');
	});

	// some expansions
	anys['Any fruit excluding Apple'] = _.pullAll(anys['Any fruit'].slice(0), ['Apple']);
	anys['Any fruit excluding Apple or Fortified Pumpkin'] = _.pullAll(anys['Any fruit'].slice(0), ['Apple', 'Fortified Pumpkin']);
	anys['Any seafood excluding Hearty Salmon or Porgy'] = _.chain(anys['Any seafood'])
		.clone()
		.pull('Hearty Salmon')
		.pullAll(anys['Any porgy'])
		.value();

	anysText['Any fruit excluding Apple'] = ['Any fruit'];
	anysText['Any fruit excluding Apple or Fortified Pumpkin'] = ['Any fruit'];
	anysText['Any seafood excluding Hearty Salmon or Porgy'] = ['Any seafood'];
	anysText['Any snail or crab'] = ['Any snail', 'Any crab'];
	[
		'Any Hearty Radish or Big Hearty Radish',
		'Any Hyrule Bass or Staminoka Bass',
		'Any Wildberry or Apple',
		'Any carrot or Fortified Pumpkin',
		'Any porgy or Hearty Blueshell Snail',
	].forEach((k) => anysText[k] = [k]);
	anysText['Any vegetable or herb'].push('Any vegetable', 'Any herb');

	// REVIEW anys vs anysText
	// if(!_.isEqual(_.keys(anys), _.keys(anysText))) {
	// 	console.log('anys', _.pullAll(_.keys(anys), _.keys(anysText)));
	// 	console.log('anysText', _.pullAll(_.keys(anysText), _.keys(anys)));
	// 	throw new Error('mismatch between anys and anysText');
	// }

	// TODO vet utils.ANYS against anys object
	//  - so we can remove things from utils.ANYS that aren't needed anymore

	const actualAnys = _.chain(dishes)
		.map('generic')
		.compact()
		.flatten()
		.sort()
		.sortedUniq()
		.filter((n) => _.startsWith(n, 'Any'))
		.value();

	_.chain(actualAnys)
		.reject((name) => name in anys)
		.filter((name) => _.includes(name, ' or '))
		.forEach((name) => {
			const list = [];
			const allFound = name.substr(4).split(' or ').every((n) => {
				if(_.some(database.food, { name: n })) {
					list.push(n);
					return true;
				}
				else if(anys['Any ' + n]) {
					Array.prototype.push.apply(list, anys['Any ' + n]);
					return true;
				}
				return false;
			});
			if(allFound) {
				anys[name] = list;
			}
		})
		.value();

	_.keys(anys).forEach((any) => {
		if(!(any in anysText)) throw new Error(any + ' is not in anysText');
	});

	utils.verifyString(
		// 'Any variety' will be handled manually
		_.reject(actualAnys, (n) => _.startsWith(n, 'Any variety of four different')),
		// 'Any snail' isn't used directly, but it's an interesting filter to keep
		// added grain and dairy specifically for filters on the ui
		_.pullAll(_.keys(anys), ['Any bass', 'Any snail', 'Any grain', 'Any dairy']),
		'did not account for every any case'
	);
	_.forEach(anys, (ingredients, name) => utils.checkIngredients({ name, ingredients }, database.food));
}

function handleFood(food, sources) {
	sources = _.groupBy(sources, 'name');

	food.forEach((item) => {
		item.sources = sources[item.name];
		item.localImage = collectProperty(item, 'localImage');
		item.found = _.chain(item.sources).map('found').flatten().compact().uniq().value();
		item.number = collectProperty(item, 'number');
		delete item.sources;

		utils.checkString(item, 'name', 'UNKNOWN FOOD NAME');
		utils.checkNumber(item, 'hearts', 0, 8, item.name);
		utils.checkString(item, 'effect', item.name);
		utils.checkString(item, 'potency', item.name);
		utils.checkString(item, 'duration', item.name);
		utils.checkString(item, 'localImage', item.name);
		utils.checkString(item, 'found', item.name);
		utils.checkNumber(item, 'number', 1, 149, item.name);
	});
	// all numbers must be unique
	if(!_.isEqual(_.map(food, 'number'), _.uniq(_.map(food, 'number'))))
		throw new Error('food numbers are not unique');

	const missingImage = _.chain(food).reject('localImage').map('name').value();
	if(missingImage.length) {
		console.log('missing image', missingImage.length, missingImage);
	}

	// stats on food; which properties are used
	// const keys = _.chain(food).values().map(_.keys).flatten().sort().sortedUniq().value();
	// const maxKeyLength = _.chain(keys).map(_.size).max().value();
	// keys.forEach((prop) => {
	// 	const has = _.padStart(_.reject(food, (f) => f[prop] === null || f[prop] === undefined).length, 3);
	// 	const hasnot = _.padStart(_.filter(food, (f) => f[prop] === null || f[prop] === undefined).length, 3);
	// 	const highlight = (+hasnot && !_.includes(['effect', 'examples', 'generic', 'ingredients', 'notes'], prop) ? '*' : '');
	// 	console.log(_.padEnd(prop, maxKeyLength), 'without', hasnot, 'with', has, highlight);
	// });
}

// this is the meat of the whole combing process
function handleIngredients(dish, anys) {
	let genericExamples = _.chain(dish.examples)
		.filter('generic')
		.uniqWith(_.isEqual)
		.value();
	if(genericExamples.length > 1) {
		// if there are still too many, then we can use the generic of the base example
		genericExamples = genericExamples.filter((ge) => ge.name === dish.name);
	}
	if(genericExamples.length > 1) {
		// if there are multiple generics with different lengths, then we can assume the shortest of them is the best
		// make sure that it is representative of all generics
		const shortestLen = _.min(_.map(genericExamples, 'generic.length'));
		const shortestList = _.filter(genericExamples, (ge) => ge.generic.length === shortestLen);
		if(shortestList.length !== 1) throw new Error('shortest generic, gotta do a different algorithm');
		if(_.some(genericExamples, (e) => _.difference(shortestList[0].generic, e.generic).length)) {
			console.log(shortestList[0]);
			console.log(genericExamples);
			throw new Error('shortest is not a subset of the examples');
		}
		genericExamples = shortestList;
	}

	// the best generic examples have been picked


	let exactExamples = _.chain(dish.examples)
		.filter('ingredients')
		.forEach((e) => e.ingredients = _.sortBy(e.ingredients, _.identity))
		.uniqWith(_.isEqual)
		.value();
	// group by ingredients, collect values
	if(!_.isEqual(exactExamples, _.uniqWith(exactExamples, (a, b) => _.isEqual(a.ingredients, b.ingredients)))) {
		exactExamples = _.chain(exactExamples)
			.groupBy((e) => e.ingredients.join('|'))
			.values()
			.map((list) => {
				if(list.length === 1) return list[0];

				// TODO verify that there are no other props?
				const obj = { name: _.uniq(_.map(list, 'name')).join('|'), sources: list };
				return {
					name: collectProperty(obj, 'name'),
					ingredients: list[0].ingredients,
					notes: collectProperty(obj, 'notes', { list: true }).join(' - '),
					effect: collectProperty(obj, 'effect'),
				};
			})
			.value();
	}

	// all the exact examples have been collected


	// if there still is no generic, pick one from the exact
	if(genericExamples.length === 0) {
		const shortestLen = _.min(_.map(exactExamples, 'ingredients.length'));
		const shortestList = _.filter(exactExamples, (e) => e.ingredients.length === shortestLen);
		if(shortestList.length !== 1) {
			console.log(dish);
			throw new Error('need to update the generic examples, or just define one');
		}
		genericExamples = _.cloneDeep(shortestList);
		genericExamples[0].generic = genericExamples[0].ingredients;
		delete genericExamples[0].ingredients;
	}


	// at this point we should have one and only one example
	if(genericExamples.length !== 1) {
		console.log(dish);
		// not that we cannot handle them, but just that
		if(genericExamples.length > 1) throw new Error('more generic cases than we can handle');
		else throw new Error('no generic examples');
	}
	// at this point, we should have at least one example
	if(exactExamples.length === 0) {
		exactExamples = [makeOneExample(genericExamples[0], anys)];
	}

	// finally, we have a generic case and at least one example
	dish.generic = genericExamples[0].generic;
	dish.examples = exactExamples;
}
function makeOneExample(genericExample, anys) {
	const generic = genericExample.generic;
	const example = _.cloneDeep(genericExample);
	delete example.generic;

	if(generic.length === 1 && _.startsWith(generic[0], 'Any variety of four different')) {
		const name = generic[0].replace('Any variety of four different', 'Any').replace(/s\b/g, '');
		example.ingredients = anys[name].slice(0, 4);
	}
	else {
		example.ingredients = _.uniq(generic.map((name) => (
			// if it's in the anys object, then just use the first item it finds
			// if it's not, then we can assume it's an ingredient
			(name in anys ? anys[name][0] : name)
		)).sort());
	}
	return example;
}
